//
//  AccountPhotoViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 19/01/17.
//
//

import UIKit
import MBProgressHUD

class AccountPhotoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    fileprivate var scrollView: UIScrollView!
    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var premiumView: UIView!
    fileprivate var premiumLabel: UILabel!
    fileprivate var premiumButton: UIButton!
    fileprivate var mainLabel: UILabel!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var editButton: UIButton!
    fileprivate var extrasLabel: UILabel!
    fileprivate var extrasDescriptionLabel: UILabel!
    fileprivate var collectionViewLayout: UICollectionViewFlowLayout!
    fileprivate var collectionView: UICollectionView!
    fileprivate var sendButton: UIButton!
    fileprivate var messageLabel: UILabel!
    fileprivate var items: [Picture]!
    fileprivate var itemsUpload: [String: UIImage]!
    fileprivate var maxLength: Int!
    fileprivate var isLoading: Bool!
    fileprivate var index: Int!
    fileprivate var isModal: Bool!
    fileprivate var backgroundView: BackgroundView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.scrollView = UIScrollView(frame: UIScreen.main.bounds)
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.premiumView = UIView(frame: CGRect(x: 10, y: 10, width: UIScreen.main.bounds.width - 20, height: 0))
        self.premiumButton = UIButton(frame: CGRect(x: 0, y: self.premiumView.frame.origin.y + self.premiumView.bounds.height + 10, width: 300, height: 44))
        self.premiumLabel = UILabel(frame: CGRect(x: 10, y: 10, width: self.premiumView.bounds.width - 20, height: 0))
        self.mainLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 44))
        self.avatarImageView = UIImageView(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width / 2, height: UIScreen.main.bounds.width / 2))
        self.editButton = UIButton(frame: CGRect(x: 0, y: 44 + self.avatarImageView.bounds.height, width: 80, height: 20))
        self.extrasLabel = UILabel(frame: CGRect(x: 10, y: self.editButton.frame.origin.y + self.editButton.bounds.height + 10, width: UIScreen.main.bounds.width - 20, height: 44))
        self.extrasDescriptionLabel = UILabel(frame: CGRect(x: 10, y: self.extrasLabel.frame.origin.y + self.extrasLabel.bounds.height, width: UIScreen.main.bounds.width - 20, height: 44))
        self.collectionViewLayout = UICollectionViewFlowLayout()
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: self.extrasLabel.frame.origin.y + self.extrasLabel.bounds.height, width: UIScreen.main.bounds.width, height: 110), collectionViewLayout: self.collectionViewLayout)
        self.sendButton = UIButton(frame: CGRect(x: 0, y: self.collectionView.frame.origin.y + self.collectionView.bounds.height + 10, width: 300, height: 44))
        self.messageLabel = UILabel(frame: CGRect(x: 10, y: self.sendButton.frame.origin.y + self.sendButton.bounds.height + 10, width: UIScreen.main.bounds.width - 20, height: 0))
        self.items = [Picture]()
        self.itemsUpload = [String: UIImage]()
        self.maxLength = 4
        self.isLoading = false
        self.index = 0
        self.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.loadItems))
    }
    
    convenience init(isModal: Bool) {
        self.init()
        self.isModal = isModal
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.loadItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.view.addSubview(self.scrollView)
        self.view.addSubview(self.activityIndicatorView)
        self.view.backgroundColor = .white
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Minhas Fotos", comment: "")
        if self.isModal != nil && self.isModal == true {
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem.close(self, action: #selector(tapCloseButton))]
        } else {
            self.navigationItem.leftBarButtonItems = [UIBarButtonItem.space(-10), UIBarButtonItem.back(title: "Minha Conta", target: self, action: #selector(tapBackButton))]
            self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "Minha Conta", style: .plain, target: self, action: #selector(tapBackButton))
        }

        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
        self.activityIndicatorView.startAnimating()
    }
    
    func refresh() {
        self.scrollView.addSubview(self.mainLabel)
        self.scrollView.addSubview(self.premiumView)
        self.scrollView.addSubview(self.premiumButton)
        self.scrollView.addSubview(self.avatarImageView)
        self.scrollView.addSubview(self.editButton)
        self.scrollView.addSubview(self.extrasLabel)
        self.scrollView.addSubview(self.extrasDescriptionLabel)
        self.scrollView.addSubview(self.collectionView)
        self.scrollView.addSubview(self.sendButton)
        self.scrollView.addSubview(self.messageLabel)
        
        self.premiumView.isHidden = true
        self.premiumButton.isHidden = true
        
        self.mainLabel.font = UIFont(name: "Museo-700", size: 15)
        self.mainLabel.textColor = .aecColor()
        self.mainLabel.text = NSLocalizedString("Foto Principal", comment: "").uppercased()

        self.avatarImageView.isUserInteractionEnabled = true
        self.avatarImageView.backgroundColor = .white
        self.avatarImageView.contentMode = .scaleAspectFill
        self.avatarImageView.center = CGPoint(x: self.view.center.x, y: self.avatarImageView.center.y)
        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.bounds.width / 2
        self.avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapAvatar)))
        
        self.editButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.editButton.setTitleColor(UIColor.aecColor(), for: .normal)
        self.editButton.setTitleColor(UIColor.aecColor().lighterColor(), for: .highlighted)
        self.editButton.setTitle(NSLocalizedString("Nova foto", comment:""), for: .normal)
        self.editButton.setTitle(NSLocalizedString("Nova foto", comment:""), for: .highlighted)
        self.editButton.setTitle(NSLocalizedString("Alterar", comment:""), for: .selected)
        self.editButton.setTitle(NSLocalizedString("Alterar", comment:""), for: [.selected, .highlighted])
        self.editButton.center = CGPoint(x: self.view.center.x, y: self.editButton.center.y)
        self.editButton.addTarget(self, action: #selector(self.tapAvatar), for: UIControlEvents.touchUpInside)
        
        self.extrasLabel.font = UIFont(name: "Museo-700", size: 15)
        self.extrasLabel.textColor = .aecColor()
        self.extrasLabel.text = NSLocalizedString("Fotos Extras", comment: "").uppercased()
        
        self.extrasDescriptionLabel.font = UIFont(name: "Museo-500", size: 12)
        self.extrasDescriptionLabel.textColor = .gray
        self.extrasDescriptionLabel.text = NSLocalizedString("Para adicionar fotos extras, primeiro selecione uma FOTO PRINCIPAL.", comment: "")
        self.extrasDescriptionLabel.lineBreakMode = .byWordWrapping
        self.extrasDescriptionLabel.numberOfLines = 0
        
        self.collectionViewLayout.scrollDirection = .horizontal
        self.collectionViewLayout.itemSize = CGSize(width: 80, height: 110)
        self.collectionViewLayout.minimumLineSpacing = 10
        self.collectionViewLayout.minimumInteritemSpacing = 10
        
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.collectionView.backgroundColor = .clear
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.alwaysBounceHorizontal = false
        self.collectionView.alwaysBounceVertical = false
        self.collectionView.isPagingEnabled = true
        self.collectionView.register(AccountPhotoCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        self.sendButton.isEnabled = false
        self.sendButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.sendButton.setTitleColor(UIColor.white, for: UIControlState())
        self.sendButton.setBackgroundImage(UIImage.color(UIColor.aecColor()), for: UIControlState())
        self.sendButton.setTitle(NSLocalizedString("Enviar Fotos", comment:""), for: UIControlState())
        self.sendButton.addTarget(self, action: #selector(self.tapSendButton), for: UIControlEvents.touchUpInside)
        self.sendButton.layer.cornerRadius = 5
        self.sendButton.layer.masksToBounds = true
        self.sendButton.center = CGPoint(x: self.view.center.x, y: self.sendButton.center.y)
        
        self.messageLabel.lineBreakMode = .byWordWrapping
        self.messageLabel.numberOfLines = 0
        
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: "Atenção: ", attributes: [NSFontAttributeName: UIFont(name: "Museo-500", size: 12)!, NSForegroundColorAttributeName: UIColor.aecColor()]))
        attributedString.append(NSAttributedString(string: "Quando você faz qualquer alteração em suas fotos, todas voltam ao estágio inicial de aprovação. Seu cadastro ficará sem fotos até que todas sejam aprovadas novamente.", attributes: [NSFontAttributeName: UIFont(name: "Museo-300", size: 12)!, NSForegroundColorAttributeName: UIColor.gray]))
        self.messageLabel.attributedText = attributedString
        self.messageLabel.sizeToFit()
        
        if App.shared.canUpload != nil && !App.shared.canUpload! {
            self.premiumLabel.font = UIFont(name: "Museo-300", size: 12)!
            self.premiumLabel.textColor = .gray
            self.premiumLabel.text = NSLocalizedString("Você já alterou uma foto essa semana. Se deseja alterar outra foto, deve se tornar um usuário PREMIUM, ou então esperar até a próxima semana para alterar sua foto novamente.", comment: "")
            self.premiumLabel.numberOfLines = 0
            self.premiumLabel.lineBreakMode = .byWordWrapping
            self.premiumLabel.sizeToFit()
            
            self.premiumView.isHidden = false
            self.premiumView.backgroundColor = .aecPinkColor()
            self.premiumView.addSubview(self.premiumLabel)
            self.premiumView.frame = CGRect(x: 10, y: 10, width: self.premiumView.bounds.width, height: self.premiumLabel.bounds.height + 20)
            
            self.premiumButton.isHidden = false
            self.premiumButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
            self.premiumButton.setTitleColor(UIColor.white, for: UIControlState())
            self.premiumButton.setBackgroundImage(UIImage.color(UIColor.aecColor()), for: UIControlState())
            self.premiumButton.setTitle(NSLocalizedString("Virar Premium", comment:"").uppercased(), for: UIControlState())
            self.premiumButton.addTarget(self, action: #selector(self.tapPremiumButton), for: UIControlEvents.touchUpInside)
            self.premiumButton.layer.cornerRadius = 5
            self.premiumButton.layer.masksToBounds = true
            self.premiumButton.center = CGPoint(x: self.view.center.x, y: self.premiumButton.center.y)
            
            self.avatarImageView.isUserInteractionEnabled = false
            self.collectionView.isUserInteractionEnabled = false
            
            self.premiumButton.frame.origin = CGPoint(x: self.premiumButton.frame.origin.x, y: self.premiumView.frame.origin.y + self.premiumView.bounds.height + 10)
            self.mainLabel.frame.origin = CGPoint(x: self.mainLabel.frame.origin.x, y: self.premiumButton.frame.origin.y + self.premiumButton.bounds.height + 10)
            self.avatarImageView.frame.origin = CGPoint(x: self.avatarImageView.frame.origin.x, y: self.mainLabel.frame.origin.y + self.mainLabel.bounds.height)
            self.editButton.frame.origin = CGPoint(x: self.editButton.frame.origin.x, y: self.avatarImageView.frame.origin.y + self.avatarImageView.bounds.height)
            self.extrasLabel.frame.origin = CGPoint(x: self.extrasLabel.frame.origin.x, y: self.editButton.frame.origin.y + self.editButton.bounds.height + 10)
            self.extrasDescriptionLabel.frame.origin = CGPoint(x: self.extrasDescriptionLabel.frame.origin.x, y: self.extrasLabel.frame.origin.y + self.extrasLabel.bounds.height)
            self.collectionView.frame.origin = CGPoint(x: self.collectionView.frame.origin.x, y: self.extrasLabel.frame.origin.y + self.extrasLabel.bounds.height)
            self.sendButton.frame.origin = CGPoint(x: self.sendButton.frame.origin.x, y: self.collectionView.frame.origin.y + self.collectionView.bounds.height + 10)
            self.messageLabel.frame.origin = CGPoint(x: self.messageLabel.frame.origin.x, y: self.sendButton.frame.origin.y + self.sendButton.bounds.height + 10)
        }
        
        self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.messageLabel.frame.origin.y + self.messageLabel.bounds.height + 10)
        
        if self.items.count > 1 {
            self.avatarImageView.load(self.items[0])
            self.editButton.isSelected = true
            self.collectionView.isHidden = false
            self.extrasDescriptionLabel.isHidden = true
        } else {
            self.avatarImageView.image = #imageLiteral(resourceName: "add")
            self.collectionView.isHidden = true
            self.extrasDescriptionLabel.isHidden = false
        }
        if !App.shared.canUpload! {
            self.editButton.isHidden = true
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AccountPhotoCollectionViewCell
        if self.items.count - 2 == indexPath.item {
            cell.layout(self.items[indexPath.item + 1], false)
        } else {
            cell.layout(self.items[indexPath.item + 1], true)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.index = indexPath.item + 1
        self.showAlert()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if self.index < self.items.count - 1 {
                self.items.remove(at: self.index)
                self.items.insert(Picture(image: image), at: self.index)
            } else {
                self.items.insert(Picture(image: image), at: self.items.count - 1)
            }
            if self.items.count == 6 {
                self.items.remove(at: 5)
            }
            self.itemsUpload["photo_\(self.index + 1)"] = image
            self.refresh()
            self.collectionView.reloadData()
            self.sendButton.isEnabled = true
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func loadItems() {
        if self.isLoading == false {
            self.isLoading = true
            App.shared.photos(block: { (items, maxLength, error) in
                self.backgroundView.removeFromSuperview()
                if error == nil {
                    self.items.append(contentsOf: items!)
                    self.maxLength = maxLength!
                    if self.items.count < self.maxLength! {
                        self.items.append(Picture(image: #imageLiteral(resourceName: "add")))
                    }
                    self.collectionView.reloadData()
                    self.refresh()
                } else {
                    if App.shared.net.isReachable {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        self.view.addSubview(self.backgroundView)
                    }
                }
                self.activityIndicatorView.stopAnimating()
                self.isLoading = false
            })
        }
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("Tirar foto", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        alertController.addAction(takePhotoAction)
        
        let chooseFromLibraryAction = UIAlertAction(title: NSLocalizedString("Escolher da biblioteca", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
            let imagePicker = UIImagePickerController()
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(chooseFromLibraryAction)
        
        alertController.view.tintColor = UIColor.aecColor()
        self.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = UIColor.aecColor()
    }
    
    func tapAvatar() {
        self.index = 0
        self.showAlert()
    }
    
    func tapSendButton() {
        let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = NSLocalizedString("Aguarde...", comment: "")
        hud.show(animated: true)
        UIImage.upload(images: self.itemsUpload) { (error) in
            hud.hide(animated: true)
            if error == nil {
                self.refresh()
                Util.showAlert(NSLocalizedString("Atenção", comment: ""), message: NSLocalizedString("O envio de fotos foi realizado com sucesso.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
            } else {
                Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
            }
        }
    }
    
    func tapCloseButton() {
        alert()
    }
    
    func tapPremiumButton() {
        UIApplication.topViewController()?.present(UINavigationController(rootViewController: PlanViewController()), animated: true, completion: nil)
    }
    
    func tapBackButton() {
        alert()
    }
    
    func alert() {
        if sendButton.isEnabled {
            let alertController = UIAlertController(title: NSLocalizedString("Atenção", comment: ""), message: NSLocalizedString("Se você sair dessa página suas mudanças não salvas serão perdidas. Deseja mesmo sair?", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancelar", comment: ""), style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
            }
            alertController.addAction(cancelAction)
            
            let action = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
                if self.isModal != nil && self.isModal == true {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            alertController.addAction(action)
            alertController.view.tintColor = UIColor.aecColor()
            UIApplication.topViewController()!.present(alertController, animated: true, completion: nil)
            alertController.view.tintColor = UIColor.aecColor()
        } else {
            if self.isModal != nil && self.isModal == true {
                self.dismiss(animated: true, completion: nil)
            } else {
                _ = navigationController?.popViewController(animated: true)
            }
        }
    }
}
