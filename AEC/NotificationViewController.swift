//
//  NotificationViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 13/01/17.
//
//

import UIKit

class NotificationViewController: UITableViewController {
    
    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var items: [Activity]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var isLoading: Bool!
    fileprivate var backgroundView: BackgroundView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.items = [Activity]()
        self.itemsHeight = NotificationTableViewCell.height()
        self.isLoading = false
        self.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "semNotificacao"), text: NSLocalizedString("Ops! Parece que você ainda não possui notificação. Que tal visitar uns perfis para ver se alguém te visita de volta.", comment: ""))
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.loadItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.view.addSubview(self.activityIndicatorView)
        self.view.backgroundColor = UIColor.white
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Notificações", comment: "")
        
        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 64)
        self.activityIndicatorView.startAnimating()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 64, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.alwaysBounceVertical = true
        self.tableView.register(NotificationTableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.sendSubview(toBack: self.refreshControl!)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemsHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTableViewCell
        cell.layout(self.items[indexPath.item])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.items[indexPath.row].type == "new_message" || self.items[indexPath.row].type == "message_not_read" {
            let viewController = ChatViewController(user: self.items[indexPath.row].user)
            viewController.hidesBottomBarWhenPushed = true
            UIApplication.topViewController()!.navigationController!.pushViewController(viewController, animated: true)
        } else {
            UIApplication.topViewController()!.navigationController!.pushViewController(UserViewController(user: self.items[indexPath.row].user), animated: true)
        }
    }
    
    func loadItems() {
        if self.isLoading == false {
            self.isLoading = true
            App.shared.notifications(block: { (items, error) in
                self.tableView!.backgroundView = nil
                if error == nil {
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshBadge"), object: nil)
                    if self.refreshControl!.isRefreshing {
                        self.items.removeAll()
                    }
                    self.items.append(contentsOf: items!)
                    self.tableView.reloadData()
                    if self.items.count == 0 {
                        self.tableView.backgroundView = self.backgroundView
                        self.backgroundView.isHidden = false
                    } else {
                        self.backgroundView.isHidden = true
                    }
                } else {
                    if App.shared.net.isReachable {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        if self.items.count == 0 {
                            self.tableView!.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.refresh))
                        }
                    }
                }
                self.activityIndicatorView.stopAnimating()
                self.refreshControl!.endRefreshing()
                self.isLoading = false
            })
        }
    }
    
    func refresh() {
        self.loadItems()
    }
}
