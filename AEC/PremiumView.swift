//
//  PremiumView.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class PremiumView: UICollectionReusableView {
    
    fileprivate var logoImageView: UIImageView!
    fileprivate var backgroundImageView: UIImageView!
    fileprivate var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.logoImageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 34, height: 34))
        self.backgroundImageView = UIImageView(frame: self.bounds)
        self.label = UILabel(frame: CGRect(x: 54, y: 0, width: self.bounds.width - 64, height: self.bounds.height))
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layout() {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapView)))
        self.layer.masksToBounds = true
        self.addSubview(self.backgroundImageView)
        self.addSubview(self.logoImageView)
        self.addSubview(self.label)

        self.logoImageView.image = UIImage(named: "mensagemLogoVirePremium")
        self.logoImageView.contentMode = UIViewContentMode.scaleAspectFill
        
        self.backgroundImageView.image = UIImage(named: "fundoBanner")
        self.backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill
        
        self.label.textColor = UIColor.white
        self.label.font = UIFont(name: "Museo-500", size: 15)!
        self.label.text = App.shared.premiumMessage!.uppercased()
        self.label.numberOfLines = 0
        self.label.lineBreakMode = NSLineBreakMode.byWordWrapping
    }
    
    func tapView() {
        UIApplication.topViewController()?.present(UINavigationController(rootViewController: PlanViewController()), animated: true, completion: nil)
    }
}
