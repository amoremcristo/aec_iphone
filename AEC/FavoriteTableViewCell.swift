//
//  FavoriteTableViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD

class FavoriteTableViewCell: UITableViewCell {
    
    fileprivate var user: User!
    fileprivate var view: UIView!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var nicknameLabel: UILabel!
    fileprivate var detailsLabel: UILabel!
    fileprivate var messageButton: UIButton!
    fileprivate var favoriteButton: UIButton!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.view = UIView(frame: CGRect(x: 0, y: 2.5, width: UIScreen.main.bounds.width, height: FavoriteTableViewCell.height() - 5))
        self.avatarImageView = UIImageView(frame: CGRect(x: UIScreen.main.bounds.width - 90, y: 10, width: 80, height: 80))
        self.nicknameLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 110, height: 0))
        self.detailsLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 110, height: 0))
        self.messageButton = UIButton(frame: CGRect(x: 0, y: FavoriteTableViewCell.height() - 45, width: UIScreen.main.bounds.width / 2, height: 40))
        self.favoriteButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: FavoriteTableViewCell.height() - 45, width: UIScreen.main.bounds.width / 2, height: 40))
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.image = nil
        self.favoriteButton.isHidden = false
    }
    
    func layout() {
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.backgroundColor = UIColor.backgroundColor()
        self.contentView.addSubview(self.view)
        
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.avatarImageView)
        self.view.addSubview(self.nicknameLabel)
        self.view.addSubview(self.detailsLabel)
        self.view.addSubview(self.messageButton)
        self.view.addSubview(self.favoriteButton)
        self.view.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.view.layer.shadowRadius = 4
        self.view.layer.shadowOpacity = 0.1
        self.view.layer.shadowPath = UIBezierPath(roundedRect: self.view.bounds, cornerRadius: 2.5).cgPath
        self.view.layer.shouldRasterize = true
        self.view.layer.rasterizationScale = UIScreen.main.scale
        
        self.avatarImageView.contentMode = .scaleAspectFill
        self.avatarImageView.backgroundColor = .darkGray
        self.avatarImageView.layer.cornerRadius = 2.5
        self.avatarImageView.layer.masksToBounds = true
        
        self.nicknameLabel.textColor = UIColor.black
        self.nicknameLabel.font = UIFont(name: "Museo-500", size: 18)
        
        self.detailsLabel.textColor = UIColor.gray
        self.detailsLabel.font = UIFont(name: "Museo-300", size: 12)
        self.detailsLabel.numberOfLines = 0
        self.detailsLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        self.messageButton.setImage(UIImage(named: "btMensagem")?.color(UIColor.aecColor()), for: .normal)
        self.messageButton.setImage(UIImage(named: "btMensagem")?.color(UIColor.aecColor().lighterColor()), for: .highlighted)
        self.messageButton.addTarget(self, action: #selector(self.tapMessageButton), for: .touchUpInside)
        
        self.favoriteButton.setImage(UIImage(named: "btFavoritoNormal")?.color(UIColor.aecColor()), for: .normal)
        self.favoriteButton.setImage(UIImage(named: "btFavoritoSelecionado")?.color(UIColor.aecColor()), for: .highlighted)
        self.favoriteButton.setImage(UIImage(named: "btFavoritoSelecionado")?.color(UIColor.aecColor()), for: .selected)
        self.favoriteButton.setImage(UIImage(named: "btFavoritoNormal")?.color(UIColor.aecColor()), for: [.selected, .highlighted])
        self.favoriteButton.addTarget(self, action: #selector(self.tapFavoriteButton(_:)), for: .touchUpInside)
    }
    
    func layout(_ user: User) {
        self.user = user
        
        self.avatarImageView.load(self.user!.avatar, thumb: true, placeholderImage: nil)
        
        self.nicknameLabel.text = self.user.nickname
        self.nicknameLabel.sizeToFit()
        self.nicknameLabel.frame = CGRect(x: 10, y: 10, width: self.contentView.bounds.width - 110, height: self.nicknameLabel.bounds.height)
        
        self.detailsLabel.text = self.user.details
        self.detailsLabel.sizeToFit()
        self.detailsLabel.frame = CGRect(x: 10, y: self.nicknameLabel.frame.origin.y + self.nicknameLabel.bounds.height + 10, width: self.contentView.bounds.width - 110, height: self.detailsLabel.bounds.height)
        if self.user.favorite == nil {
            self.favoriteButton.isHidden = true
        } else {
            self.favoriteButton.isHidden = false
        }
    }
    
    func tapMessageButton() {
        let viewController = ChatViewController(user: self.user)
        viewController.hidesBottomBarWhenPushed = true
        UIApplication.topViewController()!.navigationController!.pushViewController(viewController, animated: true)
    }
    
    func tapFavoriteButton(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = NSLocalizedString("Aguarde...", comment: "")
        hud.show(animated: true)
        if user.favorite == false {
            self.user.favorite({ (error) -> Void in
                if error == nil {
                    Util.showAlert(NSLocalizedString("Aguarde", comment: ""), message: NSLocalizedString("Seu pedido está sendo processado pelo sistema. Em breve sua lista de Favoritos será atualizada.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                } else {
                    Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                }
                hud.hide(animated: true)
            })
        } else {
            self.user.unfavorite({ (error) -> Void in
                if error == nil {
                    Util.showAlert(NSLocalizedString("Aguarde", comment: ""), message: NSLocalizedString("Seu pedido está sendo processado pelo sistema. Em breve sua lista de Favoritos será atualizada.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                } else {
                    Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                }
                hud.hide(animated: true)
            })
        }
    }
    
    class func height() -> CGFloat {
        return 135
    }
    
}
