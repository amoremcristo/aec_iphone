//
//  User.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import Alamofire

class User: NSObject, NSCoding {
    
    var id: Int!
    var nickname: String!
    var details: String!
    var avatar: Picture?
    var pictures: [Picture]?
    var groups: NSArray?
    var blockStatus: String?
    var completo: Bool?
    var favorite: Bool?
    var gender: Int?
    var messages: [ChatMessage]?
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.id = aDecoder.decodeObject(forKey: "id") as! NSInteger
        self.nickname = aDecoder.decodeObject(forKey: "nickname") as? String
        self.avatar = aDecoder.decodeObject(forKey: "avatar") as? Picture
        self.gender = aDecoder.decodeObject(forKey: "gender") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.nickname, forKey: "nickname")
        aCoder.encode(self.avatar, forKey: "avatar")
        aCoder.encode(self.gender, forKey: "gender")
    }
    
    init(dictionary: NSDictionary) {
        super.init()
        self.favorite = false
        self.parse(dictionary)
    }
    
    init(id: NSInteger, nickname: String, avatar: Picture?) {
        self.id = id
        self.nickname = nickname
        self.avatar = avatar
    }
    
    func detail(_ block: @escaping (_ error: String?) -> Void) {
        let url = GlobalConstants.URL + "profile/\(self.id!)/"
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        self.parse(value["data"] as! NSDictionary)
                    }
                    block(error)
                } else {
                    block(response.result.error?.localizedDescription)
                }
        }
    }
    
    func parse(_ dictionary: NSDictionary) {
        if dictionary["id"] != nil {
            if let string = dictionary["id"] as? String {
                self.id = NumberFormatter().number(from: string)!.intValue
            } else {
                self.id = dictionary["id"] as? NSInteger
            }
        }
        if (dictionary["user_id"] as? NSInteger) != nil {
            self.id = dictionary["user_id"] as? NSInteger
        }
        self.nickname = dictionary["apelido"] as? String
        if (dictionary["title"] as? String) != nil {
            self.nickname = dictionary["title"] as? String
        }
        if (dictionary["subtitle"] as? String) != nil {
            self.details = dictionary["subtitle"] as? String
        }
        if let dictionary = dictionary["pic"] as? NSDictionary {
            self.avatar = Picture(dictionary: dictionary)
        }
        if let array = dictionary["album"] as? NSArray {
            self.pictures = [Picture]()
            for dictionary in array {
                self.pictures?.append(Picture(dictionary: dictionary as! NSDictionary))
            }
        }
        if (dictionary["block_status"] as? String) != nil {
            self.blockStatus = dictionary["block_status"] as? String
        }
        if let array = dictionary["groups"] as? NSArray {
            self.groups = array
        }
        if (dictionary["sexo"] as? String) != nil {
            self.gender = NumberFormatter().number(from: dictionary["sexo"] as! String) as Int?
        }
    }
    
    func favorite(_ block: @escaping (_ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL + "favorites/add/\(self.id!)"
        Alamofire.request(URL, method: .post, encoding: JSONEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let error = Util.error(dictionary: response.result.value as! NSDictionary)
                    if error == nil {
                        self.favorite = true
                    }
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func unfavorite(_ block: @escaping (_ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL + "favorites/remove/\(self.id!)"
        Alamofire.request(URL, method: .post, encoding: JSONEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let error = Util.error(dictionary: response.result.value as! NSDictionary)
                    if error == nil {
                        self.favorite = false
                    }
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func block(_ block: @escaping (_ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL + "user/block/\(self.id!)"
        Alamofire.request(URL, method: .post, encoding: JSONEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let error = Util.error(dictionary: response.result.value as! NSDictionary)
                    if error == nil {
                        self.blockStatus = "blocking"
                    }
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func unblock(_ block: @escaping (_ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL + "user/unblock/\(self.id!)"
        Alamofire.request(URL, method: .post, encoding: JSONEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let error = Util.error(dictionary: response.result.value as! NSDictionary)
                    if error == nil {
                        self.blockStatus = "normal"
                    }
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func report(message: String?, _ block: @escaping (_ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL + "user/report/\(self.id!)"
        var parameters = Parameters()
        parameters["message"] = message
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let error = Util.error(dictionary: response.result.value as! NSDictionary)
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func messages(_ page: NSInteger, block: @escaping (_ items: [ChatMessage]?, _ nextPage: Bool?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"messages/\(self.id!)/"
        var parameters = Parameters()
        parameters["page"] = page
        Alamofire.request(URL, parameters: parameters, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        self.messages = [ChatMessage]()
                        var items: [ChatMessage]?
                        if let dictionary = value["data"] as? NSDictionary {
                            if let array = dictionary["messages"] as? NSArray {
                                items = [ChatMessage]()
                                for dictionary in array as! [NSDictionary] {
                                    let item = ChatMessage(dictionary: dictionary)
                                    items!.insert(item, at: 0)
                                    self.messages?.insert(item, at: 0)
                                }
                            }
                        }
                        block(items, (value["meta"] as! NSDictionary)["next_page"] as? Bool, nil)
                    } else {
                        block(nil, nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
}
