//
//  PictureCollectionViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class PictureCollectionViewCell: UICollectionViewCell {
    
    fileprivate var picture: Picture!
    fileprivate var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imageView = UIImageView(frame: self.bounds)
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
    
    func layout() {
        self.contentView.addSubview(self.imageView)
        
        self.imageView.layer.masksToBounds = true
        self.imageView.contentMode = .scaleAspectFit
    }
    
    func layout(_ picture: Picture) {
        self.layout(picture, false)
    }
    
    func layout(_ picture: Picture, _ thumb: Bool) {
        self.picture = picture
        self.imageView.load(self.picture, thumb: thumb, placeholderImage: nil)
        if thumb {
            self.imageView.contentMode = .scaleAspectFill
            self.imageView.backgroundColor = .white
            self.contentView.layer.cornerRadius = 2.5
            self.contentView.layer.masksToBounds = true
        }
    }
}
