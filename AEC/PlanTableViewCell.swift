//
//  PlanTableViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import StoreKit

class PlanTableViewCell: UITableViewCell {
    
    fileprivate var product: SKProduct!
    fileprivate var view: UIView!
    fileprivate var titleLabel: UILabel!
    fileprivate var detailsLabel: UILabel!
    fileprivate var monthlyLabel: UILabel!
    fileprivate var priceLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.view = UIView(frame: CGRect(x: 0, y: 2.5, width: UIScreen.main.bounds.width, height: PlanTableViewCell.height() - 5))
        self.titleLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 110, height: 0))
        self.detailsLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 110, height: 0))
        self.monthlyLabel = UILabel(frame: CGRect(x: 0, y: PlanTableViewCell.height() - 45, width: UIScreen.main.bounds.width / 2, height: 40))
        self.priceLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 110, height: 0))
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            self.view.backgroundColor = .backgroundColor()
        } else {
            self.view.backgroundColor = .white
        }
    }
    
    func layout() {
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.backgroundColor = UIColor.backgroundColor()
        self.contentView.addSubview(self.view)
        
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.detailsLabel)
        self.view.addSubview(self.monthlyLabel)
        self.view.addSubview(self.priceLabel)
        self.view.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.view.layer.shadowRadius = 4
        self.view.layer.shadowOpacity = 0.1
        self.view.layer.shadowPath = UIBezierPath(roundedRect: self.view.bounds, cornerRadius: 2.5).cgPath
        self.view.layer.shouldRasterize = true
        self.view.layer.rasterizationScale = UIScreen.main.scale
        
        self.titleLabel.textColor = UIColor.aecColor()
        self.titleLabel.font = UIFont(name: "Museo-500", size: 20)!
        self.titleLabel.textAlignment = .center
        
        self.detailsLabel.textColor = UIColor.gray
        self.detailsLabel.font = UIFont(name: "Museo-300", size: 12)!
        self.detailsLabel.text = NSLocalizedString("equivalente a", comment: "")
        self.detailsLabel.textAlignment = .center
        
        self.monthlyLabel.textColor = UIColor.white
        self.monthlyLabel.backgroundColor = UIColor.aecColor()
        self.monthlyLabel.font = UIFont(name: "Museo-500", size: 15)!
        self.monthlyLabel.textAlignment = .center
        self.monthlyLabel.layer.cornerRadius = 2.5
        self.monthlyLabel.layer.masksToBounds = true
        
        self.priceLabel.textColor = UIColor.gray
        self.priceLabel.font = UIFont(name: "Museo-300", size: 12)!
        self.priceLabel.textAlignment = .center
    }
    
    func layout(_ product: SKProduct) {
        self.product = product

        self.titleLabel.text = product.localizedTitle
        self.titleLabel.sizeToFit()
        self.titleLabel.frame = CGRect(x: 10, y: 10, width: self.contentView.bounds.width - 20, height: self.titleLabel.bounds.height)
        
        self.detailsLabel.sizeToFit()
        self.detailsLabel.frame = CGRect(x: 10, y: self.titleLabel.frame.origin.y + self.titleLabel.bounds.height + 10, width: self.contentView.bounds.width - 20, height: self.detailsLabel.bounds.height)
        
        self.monthlyLabel.text = "US$9,99/mês"
        if product.localizedTitle == "Semester Plan" || product.localizedTitle == "Plano Semestral" {
            self.monthlyLabel.text = "US$5,83/mês"
        }
        self.monthlyLabel.sizeToFit()
        self.monthlyLabel.frame = CGRect(x: 10, y: self.detailsLabel.frame.origin.y + self.detailsLabel.bounds.height + 10, width: self.monthlyLabel.bounds.width + 20, height: 34)
        self.monthlyLabel.center = CGPoint(x: self.center.x, y: self.monthlyLabel.center.y)
        
        self.priceLabel.text = NSLocalizedString("à vista US$", comment: "")+"\(product.price)"
        self.priceLabel.sizeToFit()
        self.priceLabel.frame = CGRect(x: 10, y: self.monthlyLabel.frame.origin.y + self.monthlyLabel.bounds.height + 10, width: self.contentView.bounds.width - 20, height: self.priceLabel.bounds.height)
    }
    
    class func height() -> CGFloat {
        return 145
    }
    
}
