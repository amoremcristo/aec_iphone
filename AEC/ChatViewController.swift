//
//  ChatViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import JSQMessagesViewController
import Alamofire

class ChatViewController: JSQMessagesViewController {
    
    fileprivate var user: User!
    fileprivate var avatar: JSQMessagesAvatarImage!
    fileprivate var bubbleImageOutgoing: JSQMessagesBubbleImage!
    fileprivate var bubbleImageOutgoingError: JSQMessagesBubbleImage!
    fileprivate var bubbleImageIncoming: JSQMessagesBubbleImage!
    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var status: NetworkReachabilityManager!
    fileprivate var items: [ChatMessage]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var isLoading: Bool!
    fileprivate var page: NSInteger?
    fileprivate var nextPage: Bool!
    
    init(user: User) {
        super.init(nibName: nil, bundle: nil)
        self.user = user
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.senderId = "\(Owner.shared.user!.id!)"
        self.senderDisplayName = Owner.shared.user?.nickname
        self.status = NetworkReachabilityManager()
        self.items = [ChatMessage]()
        self.itemsHeight = MessageTableViewCell.height()
        self.isLoading = false
        self.page = 1
        self.nextPage = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.loadItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.status.startListening()
        self.status.listener = { status in
            switch status {
            case .notReachable, .unknown:
                self.inputToolbar!.contentView!.rightBarButtonItem!.isEnabled = false
                break
            case .reachable(_):
                self.inputToolbar!.contentView!.rightBarButtonItem!.isEnabled = true
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.view.addSubview(self.activityIndicatorView)
        self.view.bringSubview(toFront: self.activityIndicatorView)
        self.view.backgroundColor = UIColor.backgroundColor()
        
        self.navigationItem.title = self.user.nickname
        self.navigationItem.rightBarButtonItems =  [UIBarButtonItem.space(-10), UIBarButtonItem.avatar(self.user.avatar!, target: self, action: #selector(self.tapAvatarButton))]
        
        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
        self.activityIndicatorView.color = .darkGray
        
        let imageView = UIImageView(frame: UIScreen.main.bounds)
        imageView.image = #imageLiteral(resourceName: "fundoMensagens")
        imageView.contentMode = .scaleAspectFill
        self.collectionView.backgroundView = imageView
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = .zero
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = .zero
        self.collectionView.collectionViewLayout.messageBubbleFont = UIFont(name: "Museo-500", size: 14)
//        self.collectionView.collectionViewLayout.messageBubbleTextViewTextContainerInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 25)

        self.inputToolbar.contentView!.textView!.font = UIFont.systemFont(ofSize: 14)
        self.inputToolbar.contentView!.textView.frame = CGRect(x: self.inputToolbar.contentView!.textView.frame.origin.x, y: self.inputToolbar.contentView!.textView.frame.origin.y + 5, width: self.inputToolbar.contentView!.textView.bounds.width, height: self.inputToolbar.contentView!.textView.bounds.height)
        self.inputToolbar.contentView!.textView!.placeHolder = NSLocalizedString("Digite aqui...", comment: "")
        self.inputToolbar.contentView!.textView!.tintColor = UIColor.aecColor()
        self.inputToolbar.contentView!.leftBarButtonItem = nil
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        self.bubbleImageIncoming = bubbleFactory!.incomingMessagesBubbleImage(with: .white)
        self.bubbleImageOutgoing = bubbleFactory!.outgoingMessagesBubbleImage(with: .aecYellowColor())
        self.inputToolbar.contentView!.rightBarButtonItem!.setTitle("Enviar", for: .normal)
        self.inputToolbar.contentView!.rightBarButtonItem!.titleLabel?.font = UIFont(name: "Museo-500", size: 14)
        self.inputToolbar.contentView!.rightBarButtonItem!.setTitleColor(UIColor.aecColor(), for: .normal)
        self.inputToolbar.contentView!.rightBarButtonItem!.setTitleColor(UIColor.aecColor().lighterColor(), for: .highlighted)
        
        self.automaticallyScrollsToMostRecentMessage = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name:NSNotification.Name(rawValue: "didBecomePremium"), object: nil)
    }
    
    func tapAvatarButton() {
        self.navigationController?.pushViewController(UserViewController(user: self.user), animated: true)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 20
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = self.items[indexPath.item]
        if message.sent! {
            return self.bubbleImageOutgoing
        } else {
            return self.bubbleImageIncoming
        }
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        self.loadItems()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return (items[indexPath.item]).data
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        if self.items[indexPath.item].data.senderId == "" && self.items[indexPath.item].content == "Torne-se Premium para ver essa mensagem!" {
            UIApplication.topViewController()?.present(UINavigationController(rootViewController: PlanViewController()), animated: true, completion: nil)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = self.items[indexPath.item]
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: message.date.formatTimeAgo(), attributes: [NSFontAttributeName: UIFont(name: "Museo-500", size: 10)!, NSForegroundColorAttributeName: UIColor.lightGray]))
        if self.items[indexPath.item].read == true && self.items[indexPath.item].data.senderId == NumberFormatter().string(from: NSNumber(value: Owner.shared.user!.id)) {
            attributedString.append(NSAttributedString(string: " ✓", attributes: [NSFontAttributeName: UIFont(name: "Museo-700", size: 15)!, NSForegroundColorAttributeName: UIColor.aecGreenColor()]))
        }
        return attributedString
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.textView.textColor = .black
        if self.items[indexPath.item].data.senderId == "" && self.items[indexPath.item].content == "Torne-se Premium para ver essa mensagem!" {
            cell.textView.textColor = .aecColor()
            cell.textView.text = cell.textView.text.uppercased()
        }
        return cell
    }

    func loadItems() {
        if self.isLoading == false {
            self.isLoading = true
            self.user.messages(1, block: { (items, nextPage, error) in
                if error == nil && items != nil {
                    self.items.append(contentsOf: items!)
                    self.page = self.page! + 1
                    self.nextPage = nextPage
                }
                self.finishReceivingMessage(animated: false)
                self.activityIndicatorView.stopAnimating()
                self.isLoading = false
            })
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        _ = ChatMessage(message: text, user: self.user, { (message, error) in
            if error == nil {
                self.items.append(message!)
                self.finishSendingMessage(animated: true)
                self.collectionView.reloadData()
            } else {
                Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
            }
            
        })
    }
    
    func refresh() {
        self.page = 1
        self.nextPage = false
        self.items = [ChatMessage]()
        self.loadItems()
    }
}
