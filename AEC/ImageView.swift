//
//  ImageView.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import AlamofireImage

extension UIImageView {
    func load(_ picture: Picture?) {
        self.load(picture, thumb: false, placeholderImage: nil)
    }
    
    func load(_ picture: Picture?, thumb: Bool?, placeholderImage: UIImage?) {
        if picture == nil {
            if placeholderImage != nil {
                self.image = placeholderImage
            }
            return
        }
        if picture!.image != nil {
            self.image = picture!.image
            return
        }
        var url = picture?.URL
        if thumb == true || url == nil  {
            url = picture?.thumbURL
        }
        if url == nil {
            return
        }
        self.af_setImage(withURL: URL(string: url!)!, placeholderImage: placeholderImage, filter: nil, progress: { (progress) in
        }, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.25), runImageTransitionIfCached: false, completion: { (response) in
        })
    }
}
