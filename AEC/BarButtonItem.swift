//
//  BarButtonItem.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

extension UIBarButtonItem {
    
    class func space(_ width: CGFloat) -> UIBarButtonItem {
        let itemSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        itemSpace.width = width
        return itemSpace
    }
    
    class func filter(_ target: AnyObject, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(title: NSLocalizedString("Filtrar",comment: ""), style: UIBarButtonItemStyle.plain, target: target, action: action)
    }
    
    class func close(_ target: AnyObject, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(title: NSLocalizedString("Fechar",comment: ""), style: UIBarButtonItemStyle.plain, target: target, action: action)
    }
    
    class func cancel(_ target: AnyObject, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(title: NSLocalizedString("Cancelar",comment: ""), style: UIBarButtonItemStyle.plain, target: target, action: action)
    }
    
    class func apply(_ target: AnyObject, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(title: NSLocalizedString("Aplicar",comment: ""), style: UIBarButtonItemStyle.plain, target: target, action: action)
    }
    
    class func more(_ target: AnyObject, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(image: UIImage(named: "btMais"), style: UIBarButtonItemStyle.plain, target: target, action: action)
    }
    
    class func back(title: String, target: AnyObject, action: Selector) -> UIBarButtonItem {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        button.setTitle(title, for: .normal)
        button.titleLabel!.font = UIFont(name: "Museo-300", size: 15)!
        button.addTarget(target, action: action, for: .touchUpInside)
        button.sizeToFit()
        return UIBarButtonItem.init(customView: button)
    }
    
    class func avatar(_ avatar: Picture, target: AnyObject, action: Selector) -> UIBarButtonItem {
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        imageView.backgroundColor = UIColor.white
        imageView.load(avatar, thumb: true, placeholderImage: nil)
        imageView.layer.cornerRadius = 17
        imageView.layer.masksToBounds = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: target, action: action))
        return UIBarButtonItem.init(customView: imageView)
    }
}
