//
//  Activity.swift
//  AEC
//
//  Created by Marcelo Cotrim on 13/01/17.
//
//

import UIKit

class Activity {
    
    var id: NSInteger!
    var user: User!
    var type: String?
    var date: Date!
    
    init(dictionary: NSDictionary) {
        self.parse(dictionary)
    }
    
    func parse(_ dictionary: NSDictionary) {
        if dictionary["id"] != nil {
            self.id = NumberFormatter().number(from: dictionary["id"] as! String) as NSInteger!
        }
        if dictionary["from_user_id"] != nil {
            self.user = User(id: NumberFormatter().number(from: dictionary["from_user_id"] as! String) as NSInteger!, nickname: dictionary["from_user_username"] as! String, avatar: Picture(URL: dictionary["from_user_image"] as! String))
        }
        if (dictionary["type"] as? String) != nil {
            self.type = dictionary["type"] as? String
        }
        if (dictionary["create_time"] as? String) != nil {
            var string = dictionary["create_time"] as! String
            string.characters.removeLast()
            string.characters.removeLast()
            self.date = Date(timeIntervalSince1970: Double(string)!)
        }
    }
}
