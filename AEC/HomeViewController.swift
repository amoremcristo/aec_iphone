//
//  HomeViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MapKit

class HomeViewController: UICollectionViewController, CLLocationManagerDelegate {
    
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var filterToolbar: UIToolbar!
    fileprivate var filterLabel: UILabel!
    fileprivate var filterButton: UIButton!
    fileprivate var items: [User]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var isLoading: Bool!
    fileprivate var page: NSInteger?
    fileprivate var nextPage: Bool!
    
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
        self.refreshControl = UIRefreshControl()
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.filterToolbar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 79, width: UIScreen.main.bounds.width, height: 30))
        self.filterLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 30))
        self.filterButton = UIButton()
        self.items = [User]()
        self.itemsHeight = UserCollectionViewCell.height()
        self.isLoading = false
        self.page = 1
        self.nextPage = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        App.shared.startLocationManager()
        App.shared.locationManager?.delegate = self
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Início", comment: "")
        self.navigationItem.rightBarButtonItems =  [UIBarButtonItem.filter(self, action: #selector(self.tapFilterButton))]
        
        self.view.backgroundColor = UIColor.backgroundColor()
        self.view.addSubview(self.filterToolbar)
        
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width / 2) - 15, height: self.itemsHeight)
        
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.showsVerticalScrollIndicator = false
        self.collectionView?.register(PremiumView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        self.collectionView?.register(UserCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.collectionView?.backgroundColor = UIColor.clear
        self.collectionView?.addSubview(self.activityIndicatorView)
        self.collectionView?.addSubview(self.refreshControl)
        self.collectionView?.sendSubview(toBack: self.refreshControl)
        
        self.filterToolbar.backgroundColor = UIColor.white.alpha(0.95)
        self.filterToolbar.isHidden = true
        self.filterToolbar.addSubview(self.filterLabel)
        self.filterToolbar.addSubview(self.filterButton)
        
        self.filterLabel.font = UIFont(name: "Museo-300", size: 12)
        
        self.filterButton.titleLabel?.font = UIFont(name: "Museo-500", size: 12)
        self.filterButton.setTitle(NSLocalizedString("Limpar", comment: ""), for: .normal)
        self.filterButton.setTitleColor(UIColor.aecColor(), for: .normal)
        self.filterButton.addTarget(self, action: #selector(tapCleanButton), for: .touchUpInside)
        self.filterButton.sizeToFit()
        self.filterButton.frame = CGRect(x: UIScreen.main.bounds.width - self.filterButton.bounds.width - 10, y: 0, width: self.filterButton.bounds.width, height: 30)

        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 64)
        self.activityIndicatorView.startAnimating()

        self.refreshControl!.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didSelectFilter), name:NSNotification.Name(rawValue: "didSelectFilter"), object: nil)
        if Owner.shared.filter!.isDefault != nil && !Owner.shared.filter!.isDefault {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "didSelectFilter"), object: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.loadItems()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! PremiumView
            headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 54)
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item >= self.items.count - 5 && self.nextPage == true && self.isLoading == false {
            self.loadItems()
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UserCollectionViewCell
        cell.layout(self.items[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(UserViewController(user: self.items[indexPath.item]), animated: true)
    }
    
    func loadItems() {
        if !self.isLoading {
            self.isLoading = true
            App.shared.users(self.page!) { (items, nextPage, error) in
                self.collectionView?.backgroundView = nil
                self.activityIndicatorView.stopAnimating()
                self.refreshControl!.endRefreshing()
                self.isLoading = false
                if error == nil {
                    if self.refreshControl.isRefreshing || self.page == 1 {
                        self.items.removeAll()
                    }
                    if self.page == 1 {
                        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
                        if App.shared.premiumMessage != nil {
                            layout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 54)
                        } else {
                            layout.headerReferenceSize = .zero
                        }
                    }
                    if items != nil {
                        self.items.append(contentsOf: items!)
                        self.collectionView?.reloadData()
                        self.page = self.page! + 1
                        self.nextPage = nextPage!
                        self.activityIndicatorView.center = CGPoint(x: self.activityIndicatorView.center.x, y: self.collectionView!.contentSize.height)
                    }
                } else {
                    if App.shared.net.isReachable {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        if self.items.count == 0 {
                            self.collectionView?.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.refresh))
                        }
                    }
                }
            }
        }
    }
    
    func didSelectFilter() {
        self.filterToolbar.isHidden = false
        var string = ""
        switch Owner.shared.filter!.gender! {
        case 0:
            string = "mulher"
            break
        case 1:
            string = "homem"
            break
        default:
            string = "ambos"
            break
        }
        string = string + ", \(Owner.shared.filter!.minAge!) a \(Owner.shared.filter!.maxAge!)"
        if Owner.shared.filter!.photo! {
            string = string + ", com foto"
        } else {
            string = string + ", sem foto"
        }

        self.filterLabel.text = string.uppercased()
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 40, right: 10)
        self.refresh()
    }
    
    func refresh() {
        self.refreshControl!.beginRefreshing()
        self.page = 1
        self.nextPage = false
        self.loadItems()
    }
    
    func tapFilterButton() {
        self.present(UINavigationController(rootViewController: FilterViewController()), animated: true, completion: nil)
    }
    
    func tapCleanButton() {
        self.filterToolbar.isHidden = true
        Owner.shared.filter?.setDefault()
        Owner.shared.save()
        self.refresh()
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
}
