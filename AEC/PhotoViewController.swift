//
//  PhotoViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class PhotoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    fileprivate var collectionViewLayout: UICollectionViewFlowLayout!
    fileprivate var collectionView: UICollectionView!
    fileprivate var user: User!
    fileprivate var page: Int!
    fileprivate var items: [Picture]!
    fileprivate var pageControl: UIPageControl!
    
    init(user: User, page: Int) {
        super.init(nibName: nil, bundle: nil)
        self.user = user
        self.page = page
        self.collectionViewLayout = UICollectionViewFlowLayout()
        self.collectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: self.collectionViewLayout)
        self.items = [Picture]()
        self.items.append(self.user.avatar!)
        if self.user.pictures != nil {
            self.items.append(contentsOf: self.user.pictures!)
        }
        self.pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 44, width: UIScreen.main.bounds.width, height: 44))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.hidesBarsOnTap = true
        self.navigationController?.isNavigationBarHidden = true
        self.collectionView.scrollToItem(at: IndexPath(item: self.page - 1, section: 0), at: .centeredHorizontally, animated: false)
        self.pageControl.currentPage = self.page - 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.hidesBarsOnTap = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.view.addSubview(self.collectionView)
        self.view.addSubview(self.pageControl)
        self.automaticallyAdjustsScrollViewInsets = false
        self.extendedLayoutIncludesOpaqueBars = true
    
        self.pageControl.numberOfPages = self.items.count
        
        self.collectionViewLayout.scrollDirection = .horizontal
        self.collectionViewLayout.itemSize = self.view.bounds.size
        self.collectionViewLayout.minimumInteritemSpacing = 0
        self.collectionViewLayout.minimumLineSpacing = 0
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.alwaysBounceHorizontal = false
        self.collectionView.alwaysBounceVertical = false
        self.collectionView.isPagingEnabled = true
        self.collectionView.register(PictureCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.bounds.width)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PictureCollectionViewCell
        cell.layout(self.items[indexPath.item])
        return cell
    }
}
