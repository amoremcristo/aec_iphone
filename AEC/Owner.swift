//
//  Owner.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//
import UIKit

class Owner: NSObject, NSCoding {
    
    var sessionToken: String?
    var token: String?
    var user: User?
    var email: String?
    var filter: Filter?
    
    static let shared = Owner()
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.sessionToken = aDecoder.decodeObject(forKey: "sessionToken") as? String
        self.token = aDecoder.decodeObject(forKey: "token") as? String
        self.user = aDecoder.decodeObject(forKey: "user") as? User
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.filter = aDecoder.decodeObject(forKey: "filter") as? Filter
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.sessionToken, forKey: "sessionToken")
        aCoder.encode(self.token, forKey: "token")
        aCoder.encode(self.user, forKey: "user")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.filter, forKey: "filter")
    }
    
    func save() {
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: "owner")
    }
    
    func delete() {
        UserDefaults.standard.removeObject(forKey: "owner")
    }
    
    func load() -> Bool {
        if let data = UserDefaults.standard.object(forKey: "owner") as? Data {
            let owner = NSKeyedUnarchiver.unarchiveObject(with: data) as! Owner
            self.sessionToken = owner.sessionToken
            self.token = owner.token
            self.user = owner.user
            self.email = owner.email
            self.filter = owner.filter
            if self.user == nil || self.token == nil || self.sessionToken == nil {
                return false
            }
            if self.filter == nil {
                self.filter = Filter()
            }
            return true
        }
        return false
    }
}
