//
//  Color.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

extension UIColor {
    class func aecColor() -> UIColor {
        return UIColor(red: 236/255, green: 26/255, blue: 77/255, alpha: 1)
    }
    class func aecPinkColor() -> UIColor {
        return UIColor(red: 255/255, green: 234/255, blue: 231/255, alpha: 1)
    }
    class func aecYellowColor() -> UIColor {
        return UIColor(red: 254/255, green: 247/255, blue: 202/255, alpha: 1)
    }
    class func aecGreenColor() -> UIColor {
        return UIColor(red: 117/255, green: 183/255, blue: 42/255, alpha: 1)
    }
    class func backgroundColor() -> UIColor {
        return UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)
    }
    
    func alpha(_ alpha: CGFloat) -> UIColor {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }
    
    func lighterColor() -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: min(b * 1.3, 1.0), alpha: a)
    }
    
    func darkerColor() -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return UIColor(hue: h, saturation: s, brightness: b * 0.75, alpha: a)
    }
}

