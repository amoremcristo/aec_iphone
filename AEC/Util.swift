//
//  Util.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import Social
import Alamofire
import AVFoundation
import AssetsLibrary

class Util {
    
    class func generateToken(email: String, password: String) {
        let string = email + ":" + password
        let dataToEncode = string.data(using: String.Encoding.utf8)
        let encodedData = dataToEncode?.base64EncodedData(options: NSData.Base64EncodingOptions.lineLength64Characters)
        let encodedString = String(data: encodedData!, encoding: String.Encoding.utf8)
        Owner.shared.sessionToken = encodedString
        Owner.shared.email = email
    }
    
    class func distanceToString(_ distance: Double) -> String {
        if distance >= 1000 {
            let km = distance / 1000
            return String(format: "%.1fkm", km)
        } else {
            return String(format: "%.1fm", distance)
        }
    }
    
    class func timeElapsed(_ date: Date) -> String {
        let seconds: TimeInterval = Date().timeIntervalSince(date)
        var string: String
        if seconds < 60 {
            string = NSLocalizedString("Now", comment: "")
        } else if seconds < 60 * 60 {
            let minutes = Int(seconds / 60)
            string = String(format: "%d %@", minutes, (minutes > 1) ? "mins" : "min")
        } else if seconds < 24 * 60 * 60 {
            let hours = Int(seconds / (60 * 60))
            string = String(format: "%d %@", hours, (hours > 1) ? "hours" : "hour")
        } else {
            let days = Int(seconds / (24 * 60 * 60))
            string = String(format: "%d %@", days, (days > 1) ? "days" : "day")
        }
        return string
    }
    
    class func errorAlert(_ viewController: UIViewController, message: String) {
        let alertController = UIAlertController(title: NSLocalizedString("Atenção", comment: ""), message: message, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
        }
        alertController.addAction(cancelAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    class func error(_ response: DataResponse<Any>) -> String? {
        if (response.result.error as! NSError).code == 500 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sessionExpired"), object: nil)
            return "Erro ao acessar os dados pedidos. Por favor refaça seu login."
        } else {
            var string: String? = response.result.error?.localizedDescription
            if let data = response.data {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    string = json!["errors"] as? String
                }
            }
            return string
        }
    }
    
    class func error(dictionary: NSDictionary) -> String? {
        if let dictionary = dictionary["errors"] as? NSDictionary {
            if let string = dictionary["auth_error"] as? String {
                return string
            }
        }
        if let string = dictionary["errors"] as? String {
            return string
        }
        return nil
    }
    
    class func alertDictionary(_ string: String?) -> [String: String?]? {
        return string == nil ? nil : ["title": "Atenção", "message": string]
    }
    
    class func errorDictionary(_ string: String?) -> [String: String?]? {
        return string == nil ? nil : ["title": "Ops! Ocorreu um erro", "message": string]
    }
    
    class func shapeLayer(_ fromX: CGFloat!, fromY: CGFloat!, toX: CGFloat!, toY: CGFloat!, width: CGFloat!, color: UIColor!) -> CAShapeLayer {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: fromX, y: fromY))
        path.addLine(to: CGPoint(x: toX, y: toY))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = width
        shapeLayer.fillColor = UIColor.clear.cgColor
        
        return shapeLayer
    }

    class func generateThumbImage(_ url: URL?) -> UIImage? {
        if url == nil {
            return nil
        }
        let asset: AVAsset = AVAsset(url: url!)
        let assetImgGenerate: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        do {
            let image: CGImage = try assetImgGenerate.copyCGImage(at: CMTimeMake(1, 30), actualTime: nil)
            return UIImage(cgImage: image)
        } catch {

            return nil
        }
    }
    
    class func viewToImage(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        return image!
    }
    
    class func alert(_ dictionary: NSDictionary) {
        if dictionary["alert_message"] != nil {
            let alertController = UIAlertController(title: NSLocalizedString("Amor em Cristo", comment: ""), message: dictionary["alert_message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
            if let string = dictionary["alert_cancel_label"] as? String {
                let cancelAction = UIAlertAction(title: string, style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
                }
                alertController.addAction(cancelAction)
            }
            if let string = dictionary["alert_ok_label"] as? String {
                let action = UIAlertAction(title: string, style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
                    if dictionary["alert_ok_url"] != nil {
                        if dictionary["alert_ok_label"] as! String == "Adicionar fotos" {
                            UIApplication.topViewController()?.present(UINavigationController(rootViewController: AccountPhotoViewController(isModal: true)), animated: true, completion: nil)
                        } else {
                            UIApplication.topViewController()!.present(UINavigationController(rootViewController: WebViewController(title: nil, url: dictionary["alert_ok_url"] as? String)), animated: true, completion: nil)
                        }
                    }
                }
                alertController.addAction(action)
            }
            alertController.view.tintColor = UIColor.aecColor()
            UIApplication.topViewController()!.present(alertController, animated: true, completion: nil)
            alertController.view.tintColor = UIColor.aecColor()
        }
    }
    
    class func showAlert(_ title: String?, message: String?, cancel: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: cancel, style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
        }
        alertController.addAction(cancelAction)
        alertController.view.tintColor = UIColor.aecColor()
        UIApplication.topViewController()!.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = UIColor.aecColor()
    }
    
    class func clearTempFolder() {
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: tempFolderPath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: NSTemporaryDirectory() + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
}

extension UIScreen {
    
    enum SizeType: CGFloat {
        case unknown = 0.0
        case iPhone4 = 960.0
        case iPhone5 = 1136.0
        case iPhone6 = 1334.0
        case iPhone6Plus = 1920.0
    }
    
    var type: SizeType {
        let height = nativeBounds.height
        guard let type = SizeType(rawValue: height) else { return .unknown }
        return type
    }
}


extension Date {
    func formatTimeAgo() -> String {
        let dfHorario = Foundation.DateFormatter()
        dfHorario.dateFormat = "HH:mm"
        
        let dfData = Foundation.DateFormatter()
        dfData.dateFormat = "dd/MM/yyyy"
        
        let todayDate = Date()
        var ti = self.timeIntervalSince(todayDate)
        ti = ti * -1
        
        if (ti < 86400) { //1 dia
            return dfHorario.string(from: self)
        
        } else if ti < 2629743 {
            let diff = round(ti / 60 / 60 / 24)
            if diff <= 2 {
                return "ONTEM"
            } else {
                return dfData.string(from: self)
            }
        } else {
            return dfData.string(from: self)
        }
    }
}

extension Array where Element: Equatable {
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

extension UILabel
{
    func setLineHeight(lineHeight: CGFloat)
    {
        let text = self.text
        if let text = text
        {
            
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            
            style.lineSpacing = lineHeight
            attributeString.addAttribute(NSParagraphStyleAttributeName,
                                         value: style,
                                         range: NSMakeRange(0, text.characters.count))
            
            self.attributedText = attributeString
        }
    }
}


