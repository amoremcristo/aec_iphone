//
//  Filter.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//
import UIKit

class Filter: NSObject, NSCoding {
    
    var minAge: NSInteger!
    var maxAge: NSInteger!
    var photo: Bool!
    var gender: NSInteger!
    var isDefault: Bool!
    
    required init?(coder aDecoder: NSCoder) {
        self.minAge = aDecoder.decodeObject(forKey: "minAge") as! NSInteger
        self.maxAge = aDecoder.decodeObject(forKey: "maxAge") as! NSInteger
        self.photo = aDecoder.decodeObject(forKey: "photo") as! Bool
        self.gender = aDecoder.decodeObject(forKey: "gender") as! NSInteger
        self.isDefault = aDecoder.decodeObject(forKey: "isDefault") as! Bool
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.minAge, forKey: "minAge")
        aCoder.encode(self.maxAge, forKey: "maxAge")
        aCoder.encode(self.photo, forKey: "photo")
        aCoder.encode(self.gender, forKey: "gender")
        aCoder.encode(self.isDefault, forKey: "isDefault")
    }
    
    override init() {
        super.init()
        self.setDefault()
    }
    
    func setDefault() {
        self.isDefault = true
        self.minAge = 18
        self.maxAge = 90
        self.photo = true
        self.gender = 2
        if Owner.shared.user!.gender != nil {
            if Owner.shared.user!.gender == 1 {
                self.gender = 0
            } else {
                self.gender = 1
            }
        }
    }
}
