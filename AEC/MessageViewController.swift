//
//  MessageViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class MessageViewController: UITableViewController {

    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var items: [Message]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var isLoading: Bool!
    fileprivate var page: NSInteger?
    fileprivate var nextPage: Bool!
    fileprivate var backgroundView: BackgroundView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.items = [Message]()
        self.itemsHeight = MessageTableViewCell.height()
        self.isLoading = false
        self.page = 1
        self.nextPage = false
        self.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "semMensagens"), text: NSLocalizedString("Ainda não há mensagens.", comment: ""))
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.loadItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.view.addSubview(self.activityIndicatorView)
        self.view.backgroundColor = UIColor.white
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Mensagens", comment: "")
        
        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 64)
        self.activityIndicatorView.startAnimating()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 64, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.alwaysBounceVertical = true
        self.tableView.register(MessageTableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.sendSubview(toBack: self.refreshControl!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name:NSNotification.Name(rawValue: "didBecomePremium"), object: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemsHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item >= self.items.count - 5 && self.nextPage == true && self.isLoading == false {
            self.loadItems()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MessageTableViewCell
        cell.layout(self.items[indexPath.item])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = self.items[indexPath.row]
        message.unread = false
        let viewController = ChatViewController(user: self.items[indexPath.row].user)
        viewController.hidesBottomBarWhenPushed = true
        UIApplication.topViewController()!.navigationController!.pushViewController(viewController, animated: true)
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func loadItems() {
        if self.isLoading == false {
            self.isLoading = true
            App.shared.messages(self.page!, block: { (items, nextPage, error) in
                self.tableView!.backgroundView = nil
                if error == nil {
                    if self.refreshControl!.isRefreshing || self.page == 1 {
                        self.items.removeAll()
                    }
                    self.items.append(contentsOf: items!)
                    self.tableView.reloadData()
                    self.page = self.page! + 1
                    self.nextPage = nextPage
                    if self.items.count == 0 {
                        self.tableView.backgroundView = self.backgroundView
                        self.backgroundView.isHidden = false
                    } else {
                        self.backgroundView.isHidden = true
                    }
                } else {
                    if App.shared.net.isReachable {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        if self.items.count == 0 {
                            self.tableView!.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.refresh))
                        }
                    }
                }
                self.activityIndicatorView.stopAnimating()
                self.refreshControl!.endRefreshing()
                self.isLoading = false
            })
        }
    }

    func refresh() {
        self.page = 1
        self.nextPage = false
        self.loadItems()
    }
}
