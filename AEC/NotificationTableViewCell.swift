//
//  NotificationTableViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 13/01/17.
//
//

import UIKit

class NotificationTableViewCell: UITableViewCell{
    
    fileprivate var notification: Activity!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var label: UILabel!
    fileprivate var dateLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.avatarImageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 44, height: 44))
        self.label = UILabel(frame: CGRect(x: 64, y: 10, width: UIScreen.main.bounds.width - 74, height: 64))
        self.dateLabel = UILabel()
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.image = nil
        self.label.frame = CGRect(x: 64, y: 10, width: UIScreen.main.bounds.width - 74, height: 64)
    }
    
    func layout() {
        self.contentView.addSubview(self.avatarImageView)
        self.contentView.addSubview(self.label)
        self.contentView.addSubview(self.dateLabel)
        
        self.avatarImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.avatarImageView.backgroundColor = UIColor.darkGray
        self.avatarImageView.layer.cornerRadius = 22
        self.avatarImageView.layer.masksToBounds = true
        
        self.label.numberOfLines = 0
        self.label.lineBreakMode = .byWordWrapping
        self.label.setLineHeight(lineHeight: 2.5)
        
        self.dateLabel.textColor = UIColor.gray
        self.dateLabel.font = UIFont(name: "Museo-300", size: 10)
    }
    
    func layout(_ notification: Activity) {
        self.notification = notification
        
        self.avatarImageView.load(self.notification.user!.avatar, thumb: true, placeholderImage: nil)

        var string = ""
        if self.notification.type == "profile_visited" {
            string = " visitou seu perfil."
        } else if self.notification.type == "new_message" {
            string = " te enviou uma mensagem."
        } else if self.notification.type == "marked_as_favorite" {
            string = " adicionou como favorito."
        } else if self.notification.type == "message_not_read" {
            string = " não conseguiu ler sua mensagem."
        }
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: self.notification.user.nickname, attributes: [NSFontAttributeName: UIFont(name: "Museo-500", size: 14)!, NSForegroundColorAttributeName: UIColor.black]))
        attributedString.append(NSAttributedString(string: string, attributes: [NSFontAttributeName: UIFont(name: "Museo-300", size: 14)!, NSForegroundColorAttributeName: UIColor.darkGray]))
        self.label.attributedText = attributedString
        self.label.sizeToFit()
        
        self.dateLabel.text = self.notification.date.formatTimeAgo()
        self.dateLabel.sizeToFit()
        self.dateLabel.frame = CGRect(x: 64, y: self.label.frame.origin.y + self.label.bounds.height + 3, width: UIScreen.main.bounds.width - 70, height: self.dateLabel.bounds.height)
    }
    
    class func height() -> CGFloat {
        return 64
    }
}
