//
//  WebViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD

class WebViewController: UIViewController, UIWebViewDelegate {
    
    fileprivate var url: String?
    fileprivate var webView: UIWebView!
    fileprivate var hud: MBProgressHUD!
    fileprivate var backgroundView: BackgroundView!
    
    init(title: String?, url: String?) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.url = url
        self.webView = UIWebView(frame: CGRect(x: 0, y: 30, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.refresh))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.hud != nil {
            self.hud.hide(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.extendedLayoutIncludesOpaqueBars = true
        self.view.addSubview(self.webView)
        self.view.backgroundColor = UIColor.white
        if self.url != nil {
            if App.shared.net.isReachable {
                self.webView.loadRequest(URLRequest(url: URL(string: Owner.shared.token == nil ? self.url! : self.url! + "/" + Owner.shared.token!)!))
            } else {
                self.webView.addSubview(self.backgroundView)
            }
        }
        self.webView.backgroundColor = UIColor.white
        self.webView.delegate = self
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if self.hud == nil {
            self.hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
            self.hud.mode = MBProgressHUDMode.indeterminate
            self.hud.label.text = NSLocalizedString("Aguarde...", comment: "")
            self.hud.show(animated: true)
        }
        let url = request.url
        if url?.absoluteString == GlobalConstants.callbackURL || url!.absoluteString.contains("fechar-webview") {
            if url?.lastPathComponent != "fechar-webview" {
                App.shared.recoverSession(token: url!.lastPathComponent, { (error) in
                    if error == nil {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "registerAccount"), object: nil)
                    }
                })
            } else {
                App.shared.login({ (error) in
                    if error == nil {
                        self.webView.loadRequest(URLRequest(url: URL(string: self.url! + "/" + Owner.shared.token!)!))
                    }
                })
            }
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hud.hide(animated: true)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.hud.hide(animated: true)
        Util.showAlert(NSLocalizedString("Atenção", comment: ""), message: "Ocorreu um erro inesperado", cancel: "OK")
    }
    
    func tapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func refresh() {
        if App.shared.net.isReachable {
            self.backgroundView.removeFromSuperview()
            self.webView.loadRequest(URLRequest(url: URL(string: Owner.shared.token == nil ? self.url! : self.url! + "/" + Owner.shared.token!)!))
        }
    }
}
