//
//  AccountPhotoCollectionViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 19/01/17.
//
//

import UIKit

class AccountPhotoCollectionViewCell: UICollectionViewCell {
    
    fileprivate var picture: Picture!
    fileprivate var imageView: UIImageView!
    fileprivate var editButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.width))
        self.editButton = UIButton(frame: CGRect(x: 0, y: self.bounds.width, width: self.bounds.width, height: 30))
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
        self.editButton.isSelected = false
    }
    
    func layout() {
        self.contentView.addSubview(self.imageView)
        self.contentView.addSubview(self.editButton)
        
        self.imageView.layer.masksToBounds = true
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.layer.cornerRadius = 40
        self.imageView.layer.masksToBounds = true
        
        self.editButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.editButton.setTitleColor(UIColor.aecColor(), for: .normal)
        self.editButton.setTitleColor(UIColor.aecColor().lighterColor(), for: .highlighted)
        self.editButton.setTitle(NSLocalizedString("Nova foto", comment:""), for: .normal)
        self.editButton.setTitle(NSLocalizedString("Nova foto", comment:""), for: .highlighted)
        self.editButton.setTitle(NSLocalizedString("Alterar", comment:""), for: .selected)
        self.editButton.setTitle(NSLocalizedString("Alterar", comment:""), for: [.selected, .highlighted])
        self.editButton.isUserInteractionEnabled = false
        
        if !App.shared.canUpload! {
            self.editButton.isHidden = true
        }
    }
    
    func layout(_ picture: Picture, _ isSelected: Bool) {
        self.picture = picture
        self.editButton.isSelected = isSelected
        self.imageView.load(self.picture, thumb: false, placeholderImage: nil)
    }
}
