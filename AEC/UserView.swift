//
//  UserView.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD

class UserView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    fileprivate var user: User!
    fileprivate var coverImageView: UIImageView!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var blur: UIBlurEffect!
    fileprivate var blurView: UIVisualEffectView!
    fileprivate var messageButton: UIButton!
    fileprivate var favoriteButton: UIButton!
    fileprivate var label: UILabel!
    fileprivate var collectionView: UICollectionView!
    fileprivate var collectionViewLayout: UICollectionViewFlowLayout!
    
    init(user: User) {
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width - 30))
        self.user = user
        self.coverImageView = UIImageView(frame: self.bounds)
        self.avatarImageView = UIImageView(frame: CGRect(x: 0, y: 44, width: self.bounds.width / 2, height: self.bounds.width / 2))
        self.blur = UIBlurEffect(style: UIBlurEffectStyle.dark)
        self.blurView = UIVisualEffectView(effect: self.blur)
        self.messageButton = UIButton(frame: CGRect(x: self.bounds.width - 64, y: 0, width: 44, height: 44))
        self.favoriteButton = UIButton(frame: CGRect(x: 20, y: 0, width: 44, height: 44))
        self.label = UILabel(frame: CGRect(x: 15, y: self.avatarImageView.frame.origin.y + self.avatarImageView.bounds.height, width: self.bounds.width - 30, height: self.bounds.width / 2 - 44))
        self.collectionViewLayout = UICollectionViewFlowLayout()
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: self.bounds.width - 30, width: self.bounds.width, height: 100), collectionViewLayout: self.collectionViewLayout)
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func layout() {
        self.addSubview(self.coverImageView)
        self.addSubview(self.blurView)
        self.addSubview(self.avatarImageView)
        self.addSubview(self.favoriteButton)
        self.addSubview(self.messageButton)
        self.addSubview(self.label)
        self.addSubview(self.collectionView)
        self.layer.masksToBounds = true
        
        self.blurView.frame = self.bounds
        
        self.coverImageView.load(self.user.avatar)
        self.coverImageView.backgroundColor = .darkGray
        self.coverImageView.layer.masksToBounds = true
        
        self.avatarImageView.load(self.user.avatar)
        self.avatarImageView.isUserInteractionEnabled = true
        self.avatarImageView.backgroundColor = .white
        self.avatarImageView.contentMode = .scaleAspectFill
        self.avatarImageView.center = CGPoint(x: self.center.x, y: self.avatarImageView.center.y)
        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.bounds.width / 2
        self.avatarImageView.layer.borderWidth = 1
        self.avatarImageView.layer.borderColor = UIColor.white.cgColor
        self.avatarImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapAvatar)))
        
        self.favoriteButton.center = CGPoint(x: self.favoriteButton.center.x, y: self.avatarImageView.center.y)
        self.favoriteButton.setImage(UIImage(named: "btPerfilNaoFavoritoNormal"), for: .normal)
        self.favoriteButton.setImage(UIImage(named: "btPerfilFavoritoSelecionado"), for: .highlighted)
        self.favoriteButton.setImage(UIImage(named: "btPerfilFavoritoNormal"), for: .selected)
        self.favoriteButton.setImage(UIImage(named: "btPerfilNaoFavoritoSelecionado"), for: [.selected, .highlighted])
        self.favoriteButton.addTarget(self, action: #selector(self.tapFavoriteButton), for: .touchUpInside)

        self.messageButton.center = CGPoint(x: self.messageButton.center.x, y: self.avatarImageView.center.y)
        self.messageButton.setImage(UIImage(named: "btPerfilMensagemNormal"), for: UIControlState())
        self.messageButton.setImage(UIImage(named: "btPerfilMensagemSelecionado"), for: UIControlState.highlighted)
        self.messageButton.addTarget(self, action: #selector(self.tapMessageButton), for: .touchUpInside)
        
        let string = NSMutableAttributedString()
        let firstString = NSAttributedString(string: self.user.nickname+"\n", attributes: [NSFontAttributeName: UIFont(name: "Museo-500", size: 20)!, NSForegroundColorAttributeName: UIColor.white])
        
        var detailsString: String = self.user.details == nil ? "" : self.user.details + "\n"
        if self.user.blockStatus == "blocking" {
            detailsString = detailsString + "Você bloqueou este usuário"
        }
        let secondString = NSAttributedString(string: detailsString, attributes: [NSFontAttributeName: UIFont(name: "Museo-300", size: 12)!, NSForegroundColorAttributeName: UIColor.white])
        string.append(firstString)
        string.append(secondString)
        self.label.attributedText = string
        self.label.lineBreakMode = .byWordWrapping
        self.label.numberOfLines = 0
        
        if self.user.pictures != nil && self.user.pictures!.count > 0 {
            self.collectionViewLayout.scrollDirection = .horizontal
            self.collectionViewLayout.itemSize = CGSize(width: 80, height: 80)
            self.collectionViewLayout.minimumLineSpacing = 10
            self.collectionViewLayout.minimumInteritemSpacing = 10
            
            self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
            self.collectionView.backgroundColor = .clear
            self.collectionView.register(PictureCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
            self.collectionView.dataSource = self
            self.collectionView.delegate = self
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width + 70)
            self.coverImageView.frame = self.bounds
            self.blurView.frame = self.bounds
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.user.pictures == nil ? 0 : self.user.pictures!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PictureCollectionViewCell
        cell.layout(self.user.pictures![indexPath.item], true)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = PhotoViewController(user: self.user, page: indexPath.item + 2)
        viewController.hidesBottomBarWhenPushed = true
        UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tapMessageButton() {
        let viewController = ChatViewController(user: self.user)
        viewController.hidesBottomBarWhenPushed = true
        UIApplication.topViewController()!.navigationController!.pushViewController(viewController, animated: true)
    }
    
    func tapFavoriteButton(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = NSLocalizedString("Aguarde...", comment: "")
        hud.show(animated: true)
        if user.favorite == false {
            self.user.favorite({ (error) -> Void in
                if error == nil {
                    Util.showAlert(NSLocalizedString("Aguarde", comment: ""), message: NSLocalizedString("Seu pedido está sendo processado pelo sistema. Em breve sua lista de Favoritos será atualizada.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                } else {
                    Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                }
                hud.hide(animated: true)
            })
        } else {
            self.user.unfavorite({ (error) -> Void in
                if error == nil {
                    Util.showAlert(NSLocalizedString("Aguarde", comment: ""), message: NSLocalizedString("Seu pedido está sendo processado pelo sistema. Em breve sua lista de Favoritos será atualizada.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                } else {
                    Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                }
                hud.hide(animated: true)
            })
        }
    }
    
    func tapAvatar() {
        let viewController = PhotoViewController(user: self.user, page: 1)
        viewController.hidesBottomBarWhenPushed = true
        UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
}
