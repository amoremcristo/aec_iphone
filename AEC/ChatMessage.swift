//
//  Chat.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import JSQMessagesViewController
import Alamofire

class ChatMessage {
    
    var id: NSInteger!
    var userId: NSInteger?
    var content: String?
    var date: Date!
    var read: Bool?
    var sent: Bool?
    var data: JSQMessage!
    
    init(dictionary: NSDictionary) {
        self.parse(dictionary)
        if self.sent! {
            self.data = JSQMessage(senderId: NumberFormatter().string(from: NSNumber(value: Owner.shared.user!.id)), senderDisplayName: Owner.shared.user!.nickname, date: date, text: content!)
        } else {
            self.data = JSQMessage(senderId: "", senderDisplayName: "", date: date, text: content!)
        }
    }
    
    func parse(_ dictionary: NSDictionary) {
        if (dictionary["id"] as? NSInteger) != nil {
            self.id = dictionary["id"] as! NSInteger
        }
        if (dictionary["content"] as? String) != nil {
            self.content = dictionary["content"] as? String
        }
        self.date = DateFormatter().date(from: dictionary["date"] as! String)
        self.read = dictionary["read"] as? Bool
        self.sent = dictionary["sent"] as? Bool
    }
    
    init(message: String, user: User, _ block: @escaping (_ message: ChatMessage?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL + "messages/send/\(user.id!)"
        var parameters = Parameters()
        parameters["message"] = message
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let error = Util.error(dictionary: response.result.value as! NSDictionary)
                    self.sent = true
                    self.read = false
                    self.date = Date()
                    self.data = JSQMessage(senderId: NumberFormatter().string(from: NSNumber(value: Owner.shared.user!.id)), senderDisplayName: Owner.shared.user!.nickname, date: self.date, text: message)
                    user.messages?.insert(self, at: 0)
                    block(self, Util.alertDictionary(error))
                } else {
                    block(nil, Util.errorDictionary(response.result.error?.localizedDescription))
                }
        }
    }
}

