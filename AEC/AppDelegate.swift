//
//  AppDelegate.swift
//  AEC
//
//  Created by Marcelo Cotrim on 8/18/16.
//
//

import UIKit
import Google

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate {

    var window: UIWindow?
    var registrationOptions: [String: Any]!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureGCM()
        configureLayout()
        if launchOptions != nil {
            if let userInfo = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification] as? [String: Any] {
                openNotification(userInfo:  userInfo)
            }
        }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: MainViewController())
        window?.makeKeyAndVisible()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshBadge"), object: nil)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Create a config and set a delegate that implements the GGLInstaceIDDelegate protocol.
        let instance = GGLInstanceIDConfig.default()
        instance?.delegate = self
        #if DEBUG
            instance?.logLevel = GGLInstanceIDLogLevel.debug
        #endif
        GGLInstanceID.sharedInstance().start(with: instance)
//        #if DEBUG
//            registrationOptions = [kGGLInstanceIDRegisterAPNSOption: deviceToken, kGGLInstanceIDAPNSServerTypeSandboxOption: true]
//        #else
            registrationOptions = [kGGLInstanceIDRegisterAPNSOption: deviceToken, kGGLInstanceIDAPNSServerTypeSandboxOption: false]
//        #endif
        GGLInstanceID.sharedInstance().token(withAuthorizedEntity: GGLContext.sharedInstance().configuration.gcmSenderID, scope: kGGLInstanceIDScopeGCM, options: registrationOptions) { (deviceToken, error) in
            if deviceToken != nil {
                App.shared.deviceToken = deviceToken
                App.shared.registerDevice({ (error) in
                })
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        GCMService.sharedInstance().appDidReceiveMessage(userInfo)
        if let dictionary = userInfo["aps"] as? NSDictionary {
            application.applicationIconBadgeNumber = dictionary["badge"] as! NSInteger
        }
        if application.applicationState != .active {
            self.openNotification(userInfo: userInfo)
        }
    }
    
    func configureGCM() {
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        let gcmConfig = GCMConfig.default()
        #if DEBUG
            gcmConfig?.logLevel = GCMLogLevel.debug
        #endif
        gcmConfig?.receiverDelegate = self
        GCMService.sharedInstance().start(with: gcmConfig)
    }
    
    func onTokenRefresh() {
        // A rotation of the registration tokens is happening, so the app needs to request a new token.
        GGLInstanceID.sharedInstance().token(withAuthorizedEntity: GGLContext.sharedInstance().configuration.gcmSenderID, scope: kGGLInstanceIDScopeGCM, options: registrationOptions) { (deviceToken, error) in
            if deviceToken != nil {
                App.shared.deviceToken = deviceToken
                App.shared.registerDevice({ (error) in
                })
            }
        }
    }
    
    func configureLayout() {
        let dictionary: [String: AnyObject] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "Museo-500", size: 20)!]
        UINavigationBar.appearance().titleTextAttributes = dictionary
        UINavigationBar.appearance().barTintColor = UIColor.aecColor()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UIActivityIndicatorView.appearance().color = .darkGray
        UIRefreshControl.appearance().tintColor = UIColor.aecColor()
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont(name: "Museo-300", size: 15)!], for: UIControlState())
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray, NSFontAttributeName: UIFont(name: "Museo-300", size: 10)!], for: UIControlState.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.aecColor(), NSFontAttributeName: UIFont(name: "Museo-300", size: 10)!], for: UIControlState.selected)
    }
    
    func openNotification(userInfo: [AnyHashable : Any]) {
        let type = userInfo["gcm.notification.type"] as! String
        let user = User(id: NumberFormatter().number(from:  userInfo["gcm.notification.from_id"] as! String)!.intValue, nickname: userInfo["gcm.notification.from_apelido"] as! String, avatar: Picture(URL: userInfo["gcm.notification.photo_url"] as! String))
        if type == "profile_visited" {
            UIApplication.topViewController()?.navigationController?.pushViewController(UserViewController(user: user), animated: true)
        } else if type == "marked_as_favorite" {
            UIApplication.topViewController()?.navigationController?.pushViewController(UserViewController(user: user), animated: true)
        } else  {
            UIApplication.topViewController()?.navigationController?.pushViewController(ChatViewController(user: user), animated: true)
        }
    }

    func willSendDataMessage(withID messageID: String!, error: Error!) {
        if error != nil {
            // Failed to send the message.
        } else {
            // Will send message, you can save the messageID to track the message
        }
    }
    
    func didSendDataMessage(withID messageID: String!) {
        // Did successfully send message identified by messageID
    }
    
    func didDeleteMessagesOnServer() {
        // Some messages sent to this device were deleted on the GCM server before reception, likely
        // because the TTL expired. The client should notify the app server of this, so that the app
        // server can resend those messages.
    }
}

extension UIApplication {
    
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(top)
            } else if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

