//
//  TabBarController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import Alamofire

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    fileprivate var homeViewController: HomeViewController!
    var homeNavigationController: UINavigationController!
    fileprivate var favoriteViewController: FavoriteViewController!
    fileprivate var favoriteNavigationController: UINavigationController!
    fileprivate var messageViewController: MessageViewController!
    fileprivate var messageNavigationController: UINavigationController!
    fileprivate var notificationViewController: NotificationViewController!
    fileprivate var notificationNavigationController: UINavigationController!
    fileprivate var accountViewController: AccountViewController!
    fileprivate var accountNavigationController: UINavigationController!
    fileprivate var index: NSInteger!
    fileprivate var netToolbar: UIToolbar!
    fileprivate var netLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [UIUserNotificationType.badge, UIUserNotificationType.sound, UIUserNotificationType.alert], categories: nil))
        UIApplication.shared.registerForRemoteNotifications()
        
        self.view.backgroundColor = UIColor.backgroundColor()
        
        self.homeViewController = HomeViewController()
        self.homeNavigationController = UINavigationController(rootViewController: self.homeViewController)
        
        self.favoriteViewController = FavoriteViewController()
        self.favoriteNavigationController = UINavigationController(rootViewController: self.favoriteViewController)
        
        self.messageViewController = MessageViewController()
        self.messageNavigationController = UINavigationController(rootViewController: self.messageViewController)
        
        self.notificationViewController = NotificationViewController()
        self.notificationNavigationController = UINavigationController(rootViewController: self.notificationViewController)
        
        self.accountViewController = AccountViewController()
        self.accountNavigationController = UINavigationController(rootViewController: self.accountViewController)
        
        self.homeNavigationController.tabBarItem = UITabBarItem(title: NSLocalizedString("Início", comment: ""), image: UIImage(named: "tabbarHomeNormal"), selectedImage: UIImage(named: "tabbarHomeSelecionado"))
        
        self.favoriteNavigationController.tabBarItem = UITabBarItem(title: NSLocalizedString("Favoritos", comment: ""), image: UIImage(named: "tabbarFavoritosNormal"), selectedImage: UIImage(named: "tabbarFavoritosSelecionado"))
        
        self.messageNavigationController.tabBarItem = UITabBarItem(title: NSLocalizedString("Mensagens", comment: ""), image:UIImage(named: "tabbarMensagensNormal"), selectedImage: UIImage(named: "tabbarMensagensSelecionado"))
        
        self.notificationNavigationController.tabBarItem = UITabBarItem(title: NSLocalizedString("Notificações", comment: ""), image:UIImage(named: "tabbarNotificacoesNormal"), selectedImage: UIImage(named: "tabbarNotificacoesSelecionado"))
        
        self.accountNavigationController.tabBarItem = UITabBarItem(title: NSLocalizedString("Conta", comment: ""), image: UIImage(named: "tabbarPerfilNormal"), selectedImage: UIImage(named: "tabbarPerfilSelecionado"))
        
        //tabbar controller
        self.viewControllers = [self.homeNavigationController, self.favoriteNavigationController, self.messageNavigationController, self.notificationNavigationController, self.accountNavigationController]
        self.selectedViewController = self.homeNavigationController
        self.tabBar.tintColor = UIColor.aecColor()
        
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            self.tabBar.items![3].badgeValue = String(format: "%d", UIApplication.shared.applicationIconBadgeNumber)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshBadge), name:NSNotification.Name(rawValue: "refreshBadge"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sessionExpired), name:NSNotification.Name(rawValue: "sessionExpired"), object: nil)
        
        self.netLabel = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 30))
        self.netLabel.font = UIFont(name: "Museo-300", size: 12)
        self.netLabel.text = NSLocalizedString("Verifique sua conexão com a internet.", comment: "")
        self.netLabel.textAlignment = .center
        
        self.netToolbar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 79, width: UIScreen.main.bounds.width, height: 79))
        self.netToolbar.isHidden = true
        self.netToolbar.addSubview(self.netLabel)
        
        if App.shared.net.isReachable == false {
            self.netToolbar.isHidden = false
        }
        App.shared.net.listener = { status in
            if !App.shared.net.isReachable {
                self.netToolbar.isHidden = false
            } else {
                self.netToolbar.isHidden = true
            }
        }
        self.view.insertSubview(self.netToolbar, belowSubview: self.tabBar)
    }
    
    func refreshBadge() {
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            self.tabBar.items![3].badgeValue = String(format: "%d", UIApplication.shared.applicationIconBadgeNumber)
        } else {
            self.tabBar.items![3].badgeValue = nil
        }
    }
    
    func sessionExpired() {
        App.shared.logout()
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.navigationController?.popToRootViewController(animated: false)
        Util.showAlert("Ops! Ocorreu um erro", message: "Erro ao acessar os dados pedidos. Por favor refaça seu login.", cancel: "OK")
    }
}
