//
//  FavoriteViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class FavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var segmentedControl: UISegmentedControl!
    fileprivate var tableView: UITableView!
    fileprivate var othersTableView: UITableView!
    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var othersActivityIndicatorView: UIActivityIndicatorView!
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate var othersRefreshControl: UIRefreshControl!
    fileprivate var items: [User]!
    fileprivate var othersItems: [User]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var isLoading: Bool!
    fileprivate var othersIsLoading: Bool!
    fileprivate var page: NSInteger?
    fileprivate var otherPage: NSInteger?
    fileprivate var nextPage: Bool!
    fileprivate var othersNextPage: Bool!
    fileprivate var backgroundView: BackgroundView!
    fileprivate var othersBackgroundView: BackgroundView!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.segmentedControl = UISegmentedControl(items: [NSLocalizedString("Meus favoritos", comment: ""), NSLocalizedString("De quem sou favorito", comment: "")])
        self.tableView = UITableView(frame: UIScreen.main.bounds)
        self.othersTableView = UITableView(frame: UIScreen.main.bounds)
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.othersActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.refreshControl = UIRefreshControl()
        self.othersRefreshControl = UIRefreshControl()
        self.items = [User]()
        self.othersItems = [User]()
        self.itemsHeight = FavoriteTableViewCell.height()
        self.isLoading = false
        self.othersIsLoading = false
        self.page = 1
        self.otherPage = 1
        self.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "semFavoritos"), text: NSLocalizedString("Por enquanto você não adicionou nenhuma pessoa aos seus favoritos.", comment: ""))
        self.othersBackgroundView = BackgroundView(image: #imageLiteral(resourceName: "semSouFavorito"), text: NSLocalizedString("Por enquanto nenhuma pessoa te adicionou como favorito.", comment: ""))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.loadItems()
        self.loadOthersItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Favoritos", comment: "")
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.othersTableView)
        self.view.addSubview(self.activityIndicatorView)
        self.view.addSubview(self.othersActivityIndicatorView)
        self.view.addSubview(self.segmentedControl)
        self.view.backgroundColor = UIColor.backgroundColor()
        
        self.navigationItem.titleView = self.segmentedControl
        
        self.segmentedControl.layer.cornerRadius = 5
        self.segmentedControl.layer.masksToBounds = true
        self.segmentedControl.backgroundColor = UIColor.aecColor()
        self.segmentedControl.tintColor = UIColor.white
        self.segmentedControl.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width - 20, height: 30)
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Museo-300", size: 12)!, NSForegroundColorAttributeName: UIColor.white], for: UIControlState())
        self.segmentedControl.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Museo-500", size: 12)!, NSForegroundColorAttributeName: UIColor.aecColor()], for: UIControlState.selected)
        self.segmentedControl.selectedSegmentIndex = 0
        self.segmentedControl.addTarget(self, action: #selector(self.segmentChanged(_:)), for: UIControlEvents.valueChanged)
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 2.5, right: 0)
        self.tableView.backgroundColor = UIColor.backgroundColor()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.alwaysBounceVertical = true
        self.tableView.register(FavoriteTableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.addSubview(self.refreshControl)
        self.tableView.sendSubview(toBack: self.refreshControl)
        
        self.othersTableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 49 + 2.5, right: 0)
        self.othersTableView.backgroundColor = UIColor.backgroundColor()
        self.othersTableView.delegate = self
        self.othersTableView.dataSource = self
        self.othersTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.othersTableView.showsVerticalScrollIndicator = false
        self.othersTableView.alwaysBounceVertical = true
        self.othersTableView.register(FavoriteTableViewCell.self, forCellReuseIdentifier: "cell")
        self.othersTableView.addSubview(self.othersRefreshControl)
        self.othersTableView.sendSubview(toBack: self.othersRefreshControl)
        self.othersTableView.isHidden = true
        
        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
        self.activityIndicatorView.startAnimating()
        
        self.othersActivityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
        
        self.refreshControl!.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        
        self.othersRefreshControl!.addTarget(self, action: #selector(self.othersRefresh), for: UIControlEvents.valueChanged)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return self.items.count
        } else {
            return self.othersItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemsHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var items: [User]!
        if tableView == self.tableView {
            if indexPath.item >= self.items.count - 5 && self.nextPage == true && self.isLoading == false {
                self.loadItems()
            }
            items = self.items

        } else {
            if indexPath.item >= self.othersItems.count - 5 && self.othersNextPage == true && self.othersIsLoading == false {
                self.loadOthersItems()
            }
            items = self.othersItems
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FavoriteTableViewCell
        cell.layout(items[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var items: [User]!
        if tableView == self.tableView {
            items = self.items
        } else {
            items = self.othersItems
        }
        self.navigationController?.pushViewController(UserViewController(user: items[indexPath.row]), animated: true)
    }
    
    func loadItems() {
        if self.isLoading == false {
            self.isLoading = true
            App.shared.favorites(self.page!, block: { (items, nextPage, error) in
                self.tableView.backgroundView = nil
                if error == nil {
                    if self.refreshControl.isRefreshing || self.page == 1 {
                        self.items.removeAll()
                    }
                    if self.page == 1 {
                        if App.shared.premiumMessage != nil {
                            let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 54 + 2.5))
                            view.addSubview(PremiumView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 54)))
                            self.tableView.tableHeaderView = view
                        } else {
                            self.tableView.tableHeaderView = nil
                        }
                    }
                    self.items.append(contentsOf: items!)
                    self.tableView.reloadData()
                    self.page = self.page! + 1
                    self.nextPage = nextPage
                    if self.items.count == 0 {
                        self.tableView.backgroundView = self.backgroundView
                        self.backgroundView.isHidden = false
                    } else {
                        self.backgroundView.isHidden = true
                    }
                } else {
                    if App.shared.net.isReachable {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        if self.items.count == 0 {
                            self.tableView.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.refresh))
                        }
                    }
                }
                self.activityIndicatorView.stopAnimating()
                self.refreshControl!.endRefreshing()
                self.isLoading = false
            })
        }
    }
    
    func loadOthersItems() {
        if self.othersIsLoading == false {
            self.othersIsLoading = true
            App.shared.favoritesOthers(self.page!, block: { (items, nextPage, error) in
                self.othersTableView.backgroundView = nil
                if error == nil {
                    if self.othersRefreshControl.isRefreshing || self.otherPage == 1 {
                        self.othersItems.removeAll()
                    }
                    if self.otherPage == 1 {
                        if App.shared.premiumMessage != nil {
                            let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 54 + 2.5))
                            view.addSubview(PremiumView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 54)))
                            self.othersTableView.tableHeaderView = view
                        } else {
                            self.othersTableView.tableHeaderView = nil
                        }
                    }
                    self.othersItems.append(contentsOf: items!)
                    self.othersTableView.reloadData()
                    self.otherPage = self.otherPage! + 1
                    self.othersNextPage = nextPage
                    if self.othersItems.count == 0 {
                        self.othersTableView.backgroundView = self.othersBackgroundView
                        self.othersBackgroundView.isHidden = false
                    } else {
                        self.othersBackgroundView.isHidden = true
                    }
                } else {
                    if App.shared.net.isReachable {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        if self.othersItems.count == 0 {
                            self.othersTableView.backgroundView = BackgroundView(image: #imageLiteral(resourceName: "erro"), text: NSLocalizedString("Erro ao carregar.", comment: ""), target: self, action: #selector(self.othersRefresh))
                        }
                    }
                }
                self.othersActivityIndicatorView.stopAnimating()
                self.othersRefreshControl!.endRefreshing()
                self.othersIsLoading = false
            })
        }
    }
    
    func refresh() {
        self.refreshControl!.beginRefreshing()
        self.page = 1
        self.nextPage = false
        self.loadItems()
    }
    
    func othersRefresh() {
        self.othersRefreshControl!.beginRefreshing()
        self.otherPage = 1
        self.othersNextPage = false
        self.loadOthersItems()
    }
    
    func segmentChanged(_ sender: UISegmentedControl) {
        self.tableView.reloadData()
        self.othersTableView.reloadData()
        switch sender.selectedSegmentIndex {
        case 0:
            self.tableView.isHidden = false
            self.othersTableView.isHidden = true
            self.view.bringSubview(toFront: self.activityIndicatorView)
            break
        default:
            self.tableView.isHidden = true
            self.othersTableView.isHidden = false
            self.view.bringSubview(toFront: self.othersActivityIndicatorView)
            break
        }
    }
}


