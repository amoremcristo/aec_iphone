//
//  BackgroundView.swift
//  AEC
//
//  Created by Marcelo Cotrim on 12/01/17.
//
//

import UIKit

class BackgroundView: UIView {
    
    fileprivate var imageView: UIImageView!
    fileprivate var label: UILabel!
    fileprivate var button: UIButton!
    
    init(image: UIImage, text: String, target: Any?, action: Selector?) {
        super.init(frame: UIScreen.main.bounds)
        self.imageView = UIImageView(image: image)
        self.label = UILabel(frame: CGRect(x: 40, y: 0, width: UIScreen.main.bounds.width - 80, height: 0))
        self.label.text = text
        self.button = UIButton()
        if target != nil {
            self.button.isHidden = false
            self.button.addTarget(target, action: action!, for: .touchUpInside)
        } else {
            self.button.isHidden = true
        }
        self.layout()
    }
    
    convenience init(image: UIImage, text: String) {
        self.init(image: image, text: text, target: nil, action: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func layout() {
        self.addSubview(self.imageView)
        self.addSubview(self.label)
        self.addSubview(self.button)
        
        self.imageView.center = self.center
        
        self.label.textColor = UIColor.gray
        self.label.font = UIFont(name: "Museo-500", size: 14)
        self.label.numberOfLines = 0
        self.label.lineBreakMode = .byWordWrapping
        self.label.textAlignment = .center
        self.label.sizeToFit()
        self.label.center = self.center
        self.label.frame.origin = CGPoint(x: self.label.frame.origin.x, y: self.imageView.frame.origin.y + self.imageView.bounds.height + 5)
        
        self.button.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.button.setTitleColor(UIColor.aecColor(), for: .normal)
        self.button.setTitle(NSLocalizedString("Tentar novamente", comment:""), for: .normal)
        self.button.sizeToFit()
        self.button.center = self.center
        self.button.frame.origin = CGPoint(x: self.button.frame.origin.x, y: self.label.frame.origin.y + self.label.bounds.height + 5)
    }
}
