//
//  App.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import Alamofire
import MBProgressHUD
import MapKit
import StoreKit

struct GlobalConstants {
    #if DEBUG
        static let URL = "http://aec2.dev.macit.com.br/api/v1/"
        static let webURL = "http://dev.amoremcristo.com/app/"
    #else
        static let URL = "https://api.amoremcristo.com/api/v1/"
        static let webURL = "https://amoremcristo.com/app/"
    #endif
    static let callbackURL = webURL + "fechar-webview"
    static let registerURL = webURL + "novo-cadastro"
    static let forgotPasswordURL = webURL + "esqueci-minha-senha"
    static let personalURL = webURL + "meus-dados-pessoais/"
    static let profileURL = webURL + "meu-perfil"
    static let photosURL = webURL + "minhas-fotos"
    static let locationURL = webURL + "minha-localidade"
    static let passwordURL = webURL + "minha-senha"
    static let textURL = webURL + "meus-textos"
    static let paymentURL = webURL + "minha-cobranca"
    static let optionsURL = webURL + "opcoes-da-minha-conta"
    static let contactURL = webURL + "fale-conosco"
}

class App {
    
    var deviceToken: String!
    var favorites: [User]!
    var favoritesOthers: [User]!
    var messages: [Message]!
    var notifications: [Activity]!
    var photos: [Picture]!
    var premiumMessage: String?
    var canUpload: Bool?
    var locationManager: CLLocationManager?
    var net: NetworkReachabilityManager!
    
    func startLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager!.requestWhenInUseAuthorization()
        self.locationManager!.startUpdatingLocation()
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager!.stopUpdatingLocation()
    }
    
    static let shared = App()
    
    func registerDevice(_ block: @escaping (_ error: [String: String?]?) -> Void) {
        var parameters = Parameters()
        parameters["token"] = self.deviceToken
        let url = GlobalConstants.URL+"register-device/ios"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func recoverSession(token: String, _ block: @escaping (_ error: [String: String?]?) -> Void) {
        var parameters = Parameters()
        parameters["token"] = token
        let url = GlobalConstants.URL+"use-token/"
        Alamofire.request(url, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        Util.generateToken(email: (value["data"] as! NSDictionary)["email"] as! String, password: (value["data"] as! NSDictionary)["senha"] as! String!)
                        Owner.shared.token = token
                        Owner.shared.user = User(dictionary: value["data"] as! NSDictionary)
                        Owner.shared.save()
                    }
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func login(_ block: @escaping (_ error: [String: String?]?) -> Void) {
        let url = GlobalConstants.URL+"user/"
        Alamofire.request(url, headers: SessionManager.shared.headers())
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        Owner.shared.token = (value["meta"] as! NSDictionary)["token"] as? String
                        Owner.shared.user = User(dictionary: value["data"] as! NSDictionary)
                        Owner.shared.save()
                    }
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func logout() {
        Owner.shared.sessionToken = nil
        Owner.shared.token = nil
        Owner.shared.user = nil
        let data = NSKeyedArchiver.archivedData(withRootObject: Owner.shared)
        UserDefaults.standard.set(data, forKey: "lastOwner")
        Owner.shared.delete()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func users(_ page: NSInteger, block: @escaping (_ items: [User]?, _ nextPage: Bool?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"search/"
        var parameters = Parameters()
        parameters["page"] = page
        parameters["from_age"] = Owner.shared.filter!.minAge
        parameters["to_age"] = Owner.shared.filter!.maxAge
        parameters["photos"] = Owner.shared.filter!.photo! ? 1 : 0
        parameters["gender"] = Owner.shared.filter!.gender
        Alamofire.request(URL, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        Util.alert(value["meta"] as! NSDictionary)
                        self.premiumMessage = (value["meta"] as! NSDictionary)["premium_message"] as? String
                        var items: [User]?
                        if let array = value["data"] as? NSArray {
                            items = [User]()
                            for dictionary in array as! [NSDictionary] {
                                let item = User(dictionary: dictionary)
                                items!.append(item)
                            }
                        }
                        block(items, (value["meta"] as! NSDictionary)["next_page"] as? Bool, nil)
                    } else {
                        block(nil, nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func favorites(_ page: NSInteger, block: @escaping (_ items: [User]?, _ nextPage: Bool?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"favorites/"
        var parameters = Parameters()
        parameters["page"] = page
        Alamofire.request(URL, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        Util.alert(value["meta"] as! NSDictionary)
                        self.premiumMessage = (value["meta"] as! NSDictionary)["premium_message"] as? String
                        self.favorites = [User]()
                        var items: [User]?
                        if let array = value["data"] as? NSArray {
                            items = [User]()
                            for dictionary in array as! [NSDictionary] {
                                let item = User(dictionary: dictionary)
                                item.favorite = true
                                items!.append(item)
                                self.favorites?.append(item)
                            }
                        }
                        block(items, (value["meta"] as! NSDictionary)["next_page"] as? Bool, nil)
                    } else {
                       block(nil, nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func favoritesOthers(_ page: NSInteger, block: @escaping (_ items: [User]?, _ nextPage: Bool?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"favorites/me/"
        var parameters = Parameters()
        parameters["page"] = page
        Alamofire.request(URL, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value as NSDictionary):
                    Util.alert(value["meta"] as! NSDictionary)
                    self.premiumMessage = (value["meta"] as! NSDictionary)["premium_message"] as? String
                    self.favoritesOthers = [User]()
                    var items: [User]?
                    if let array = value["data"] as? NSArray {
                        items = [User]()
                        for dictionary in array as! [NSDictionary] {
                            let item = User(dictionary: dictionary)
                            items!.append(item)
                            self.favoritesOthers?.append(item)
                        }
                    }
                    block(items, (value["meta"] as! NSDictionary)["next_page"] as? Bool, nil)
                case .failure(_):
                    block(nil, nil, Util.errorDictionary(Util.error(response)))
                default:
                    block(nil, nil, nil)
                }
        }
    }
    
    func messages(_ page: NSInteger, block: @escaping (_ items: [Message]?, _ nextPage: Bool?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"messages/"
        var parameters = Parameters()
        parameters["page"] = page
        Alamofire.request(URL, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        self.messages = [Message]()
                        var items: [Message]?
                        if let array = value["data"] as? NSArray {
                            items = [Message]()
                            for dictionary in array as! [NSDictionary] {
                                let item = Message(dictionary: dictionary)
                                items!.append(item)
                                self.messages?.append(item)
                            }
                        }
                        block(items, (value["meta"] as! NSDictionary)["next_page"] as? Bool, nil)
                    } else {
                        block(nil, nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func notifications(block: @escaping (_ items: [Activity]?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"notifications/"
        Alamofire.request(URL, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        self.notifications = [Activity]()
                        var items: [Activity]?
                        if let array = (value["data"] as! NSDictionary)["data"] as? NSArray {
                            items = [Activity]()
                            for dictionary in array as! [NSDictionary] {
                                let item = Activity(dictionary: dictionary)
                                items!.append(item)
                                self.notifications?.append(item)
                            }
                        }
                        block(items, nil)
                    } else {
                        block(nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func photos(block: @escaping (_ items: [Picture]?, _ maxLength: Int?,  _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"user/photos"
        Alamofire.request(URL, encoding: URLEncoding.default, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        self.photos = [Picture]()
                        var items: [Picture]?
                        var maxLength: Int?
                        if let dictionary = value["data"] as? NSDictionary {
                            self.canUpload = dictionary["can_upload"] as? Bool
                            if let array = dictionary["photos"] as? [String] {
                                items = [Picture]()
                                for string in array {
                                    let item = Picture(URL: string)
                                    items!.append(item)
                                    self.photos?.append(item)
                                }
                            }
                            maxLength = dictionary["upload_max_length"] as? Int
                        }
                        block(items, maxLength, nil)
                    } else {
                        block(nil, nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func products(block: @escaping (_ items: Set<String>?, _ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"products/ios"
        Alamofire.request(URL, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    if error == nil {
                        var strings = [String]()
                        if let array = value["data"] as? NSArray {
                            for item in array {
                                strings.append(item as! String)
                            }
                        }
                        block(Set(strings), nil)
                    } else {
                        block(nil, Util.alertDictionary(error))
                    }
                } else {
                    block(nil, Util.errorDictionary(Util.error(response)))
                }
        }
    }
    
    func processPayment(transaction: SKPaymentTransaction, block: @escaping (_ error: [String: String?]?) -> Void) {
        let URL = GlobalConstants.URL+"products/ios/purchase"
        var parameters = Parameters()
        
        parameters["product_id"] = transaction.payment.productIdentifier
        parameters["transaction_id"] = transaction.transactionIdentifier
        if Bundle.main.appStoreReceiptURL != nil {
            parameters["status"] = 1
            parameters["receipt_data"] = try! Data(contentsOf: Bundle.main.appStoreReceiptURL!).base64EncodedString()
        } else {
            parameters["status"] = 0
        }
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: SessionManager.shared.headers())
            .validate()
            .responseJSON { response in
                if response.result.isSuccess {
                    let value = response.result.value as! NSDictionary
                    let error = Util.error(dictionary: value)
                    block(Util.alertDictionary(error))
                } else {
                    block(Util.errorDictionary(Util.error(response)))
                }
        }
    }
}
