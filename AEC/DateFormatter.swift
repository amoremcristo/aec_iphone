//
//  DateFormatter.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class DateFormatter: Foundation.DateFormatter {
    
    static let shared = DateFormatter()
    
    override init() {
        super.init()
        self.locale = NSLocale.system
        self.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
