//
//  UserViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD

class UserViewController: UITableViewController {
    
    fileprivate var user: User!
    fileprivate var headerView: UserView!
    fileprivate var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var items: [String]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var isLoading: Bool!
//    fileprivate var page: NSInteger?
//    fileprivate var nextPage: Bool!
    
    init(user: User!) {
        super.init(style: .grouped)
        self.user = user
        self.headerView = UserView(user: self.user)
        self.activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        self.items = [String]()
        self.itemsHeight = 64
        self.isLoading = false
//        self.page = 1
//        self.nextPage = false
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.view.addSubview(self.activityIndicatorView)
        self.view.backgroundColor = UIColor.backgroundColor()
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = self.user.nickname
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem.space(-15), UIBarButtonItem.more(self, action: #selector(self.tapMoreButton))]
        
        self.activityIndicatorView.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 64)
        self.activityIndicatorView.startAnimating()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        
        self.tableView.tableHeaderView = self.headerView
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.alwaysBounceVertical = true
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.sendSubview(toBack: self.refreshControl!)
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.font = UIFont(name: "Museo-500", size: 14)
        headerView.textLabel?.textColor = UIColor.black
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (self.user.groups![section] as! NSDictionary)["label"] as? String
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.user.groups == nil ? 0 : self.user.groups!.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.user.groups == nil ? 0 : ((self.user.groups![section] as! NSDictionary)["fields"] as! NSArray).count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemsHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.font = UIFont(name: "Museo-700", size: 12)
        cell.textLabel?.textColor = UIColor.aecColor()
        cell.textLabel?.text = ((((self.user.groups![indexPath.section] as! NSDictionary)["fields"] as! NSArray)[indexPath.row] as! NSDictionary)["label"] as? String)?.uppercased()
        cell.detailTextLabel?.text = (((self.user.groups![indexPath.section] as! NSDictionary)["fields"] as! NSArray)[indexPath.row] as! NSDictionary)["value"] as? String
        cell.detailTextLabel?.font = UIFont(name: "Museo-500", size: 12)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func load() {
        if self.isLoading == false {
            self.isLoading = true
            self.user.detail({ (error) in
                if error == nil {
                    self.headerView.layout()
                    self.tableView.reloadData()
                }
                self.activityIndicatorView.stopAnimating()
                self.refreshControl!.endRefreshing()
                self.isLoading = false
            })
        }
    }
    
    func tapMoreButton() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancelar", comment: ""), style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
            
        }
        alertController.addAction(cancelAction)
        if self.user.blockStatus != nil && self.user.blockStatus != "blocking" {

            let blockAction = UIAlertAction(title: NSLocalizedString("Bloquear usuário", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
                let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
                hud.mode = MBProgressHUDMode.indeterminate
                hud.label.text = NSLocalizedString("Aguarde...", comment: "")
                hud.show(animated: true)
                self.user.block({ (error) in
                    hud.hide(animated: true)
                    if error == nil {
                        self.headerView.layout()
                    } else {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    }
                })
            }
            alertController.addAction(blockAction)
        } else {
            let unblockAction = UIAlertAction(title: NSLocalizedString("Desbloquear usuário", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
                let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
                hud.mode = MBProgressHUDMode.indeterminate
                hud.label.text = NSLocalizedString("Aguarde...", comment: "")
                hud.show(animated: true)
                self.user.unblock({ (error) in
                    hud.hide(animated: true)
                    if error == nil {
                        self.headerView.layout()
                    } else {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    }
                })
            }
            alertController.addAction(unblockAction)
        }
        let reportAction = UIAlertAction(title: NSLocalizedString("Denunciar usuário", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
            
            
            let alertController = UIAlertController(title: NSLocalizedString("Denunciar usuário", comment: ""), message: NSLocalizedString("Por que você quer denunciar esse usuário?", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel) { (action: UIAlertAction) -> Void in
            }
            alertController.addTextField { (textField: UITextField) -> Void in
                textField.placeholder = NSLocalizedString("Motivo da denúncia", comment: "")
                textField.tintColor = UIColor.aecColor()
            }
            alertController.addAction(cancelAction)
            let okAction = UIAlertAction(title: NSLocalizedString("Envia denúncia", comment: ""), style: UIAlertActionStyle.default) { (action: UIAlertAction) -> Void in
                let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
                hud.mode = MBProgressHUDMode.indeterminate
                hud.label.text = NSLocalizedString("Aguarde...", comment: "")
                hud.show(animated: true)
                self.user.report(message: alertController.textFields![0].text, { (error) in
                    hud.hide(animated: true)
                    if error != nil {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    }
                })
            }
            alertController.addAction(okAction)
            alertController.view.tintColor = UIColor.aecColor()
            self.present(alertController, animated: true, completion: nil)
            alertController.view.tintColor = UIColor.aecColor()
        }
        alertController.addAction(reportAction)
        alertController.view.tintColor = UIColor.aecColor()
        self.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = UIColor.aecColor()
    }
    
    func refresh() {
        self.refreshControl!.beginRefreshing()
        if self.isLoading == false {
//            self.page = 1
//            self.nextPage = false
            self.load()
        }
    }
    
//    func removeItems() {
//        var indexPaths = [IndexPath]()
//        for item in self.items {
//            indexPaths.insert(IndexPath(item: self.items.index(of: item)!, section: 0), at: self.items.index(of: item)!)
//        }
//        self.items.removeAll()
//        self.tableView.deleteRows(at: indexPaths, with: UITableViewRowAnimation.fade)
//    }
}
