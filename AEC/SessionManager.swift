//
//  SessionManager.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import Alamofire

class SessionManager {
    
    static let shared = SessionManager()
    
    func headers() -> HTTPHeaders {
        var headers = [String: String]()
        headers["Authorization"] = "Basic " + Owner.shared.sessionToken!
        if App.shared.locationManager != nil && App.shared.locationManager?.location != nil {
            headers["Latitude"] = NSNumber(value: App.shared.locationManager!.location!.coordinate.latitude as Double).stringValue
            headers["Longitude"] = NSNumber(value: App.shared.locationManager!.location!.coordinate.longitude as Double).stringValue
        }
        return headers
    }
}


