//
//  AccountViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//
import UIKit
import MBProgressHUD

class AccountViewController: UITableViewController {
    
    var hud: MBProgressHUD!
    var items: [[[String: String]]]!
    
    init() {
        super.init(style: .grouped)
        self.items = [[["title": "Meus dados pessoais", "image": "account_personal", "URL": GlobalConstants.personalURL], ["title": "Meu perfil", "image": "account_profile", "URL": GlobalConstants.profileURL], ["title": "Minhas fotos", "image": "account_photos", "URL": GlobalConstants.photosURL], ["title": "Minha localidade", "image": "account_location", "URL": GlobalConstants.locationURL]],[["title": "Minha senha", "image": "account_password", "URL": GlobalConstants.passwordURL], ["title": "Meus textos", "image": "account_text", "URL": GlobalConstants.textURL], ["title": "Opções de conta", "image": "account_options", "URL": GlobalConstants.optionsURL]],[["title": "Fale conosco", "image": "account_contact", "URL": GlobalConstants.contactURL], ["title": "Sair", "image": "account_logout", "URL": "sair"]]]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Minha Conta", comment: "")
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.font = UIFont(name: "Museo-500", size: 14)
        cell.textLabel?.text = self.items[indexPath.section][indexPath.row]["title"]
        cell.textLabel?.textColor = .gray
        cell.imageView?.image = UIImage(named: self.items[indexPath.section][indexPath.row]["image"]!)?.color(UIColor.gray)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let string = self.items[indexPath.section][indexPath.row]["URL"]
        if self.items[indexPath.section][indexPath.row]["title"] == "Minhas fotos" {
            let viewController = AccountPhotoViewController()
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if string != "sair" {
            let viewController = WebViewController(title: nil, url: self.items[indexPath.section][indexPath.row]["URL"]!)
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            self.logOut()
        }
    }
    
    func logOut() {
        App.shared.logout()
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.tabBarController?.navigationController?.view.layer.add(transition, forKey: nil)
        _ = self.tabBarController?.navigationController?.popToRootViewController(animated: false)
    }
}
