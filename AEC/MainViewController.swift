//
//  MainViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD
import Alamofire
import AlamofireNetworkActivityIndicator

class MainViewController: UIViewController, UITextFieldDelegate {//, RegisterViewControllerDelegate {
    
    fileprivate var backgroundImageView: UIImageView!
    fileprivate var imageView: UIImageView!
    fileprivate var nameImageView: UIImageView!
    fileprivate var descriptionLabel: UILabel!
    fileprivate var emailTextField: UITextField!
    fileprivate var passwordTextField: UITextField!
    fileprivate var loginButton: UIButton!
    fileprivate var forgotPasswordButton: UIButton!
    fileprivate var label: UILabel!
    fileprivate var signUpButton: UIButton!

    init() {
        super.init(nibName: nil, bundle: nil)
        self.backgroundImageView = UIImageView(frame: UIScreen.main.bounds)
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 44))
        self.descriptionLabel = UILabel(frame: CGRect(x: 15, y: UIScreen.main.bounds.height - 44, width: UIScreen.main.bounds.width - 40, height: 44))
        self.loginButton =  UIButton(frame: CGRect(x: 15, y: UIScreen.main.bounds.height / 2 - 22, width: UIScreen.main.bounds.width - 30, height: 44))
        self.passwordTextField = UITextField(frame: CGRect(x: 15, y: self.loginButton.frame.origin.y - 50, width: UIScreen.main.bounds.width - 30, height: 40))
        self.emailTextField = UITextField(frame: CGRect(x: 15, y: self.passwordTextField.frame.origin.y - 45, width: UIScreen.main.bounds.width - 30, height: 40))
        self.forgotPasswordButton =  UIButton(frame: CGRect(x: 0, y: self.loginButton.frame.origin.y + self.loginButton.bounds.height + 15, width: UIScreen.main.bounds.width, height: 30))
        self.label = UILabel(frame: CGRect(x: 0, y: self.forgotPasswordButton.frame.origin.y + self.forgotPasswordButton.bounds.height + 30, width: UIScreen.main.bounds.width, height: 44))
        self.signUpButton =  UIButton(frame: CGRect(x: 15, y: self.label.frame.origin.y + self.label.bounds.height + 15, width: UIScreen.main.bounds.width - 30, height: 44))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkActivityIndicatorManager.shared.isEnabled = true
        App.shared.net = NetworkReachabilityManager()
        App.shared.net.startListening()
        if Owner.shared.load() == true {
            self.login()
        }
        self.layout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        self.tapView()
        if let data = UserDefaults.standard.object(forKey: "lastOwner") as? Data {
            let owner = NSKeyedUnarchiver.unarchiveObject(with: data) as! Owner
            self.emailTextField.text = owner.email
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func layout() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapView))
        self.view.addGestureRecognizer(tap)
        self.view.addSubview(self.backgroundImageView)
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.emailTextField)
        self.view.addSubview(self.passwordTextField)
        self.view.addSubview(self.loginButton)
        self.view.addSubview(self.forgotPasswordButton)
        self.view.addSubview(self.label)
        self.view.addSubview(self.signUpButton)
        
        self.backgroundImageView.image = UIImage(named: "fundoLogin")
        
        self.imageView.image = UIImage(named: "logo")
        self.imageView.sizeToFit()
        self.imageView.center = CGPoint(x: self.view.center.x, y: self.imageView.center.y)

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 44))
        imageView.image = UIImage(named: "username")?.color(UIColor.white)
        imageView.contentMode = UIViewContentMode.center
        
        self.emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        self.emailTextField.leftViewMode = UITextFieldViewMode.always
        self.emailTextField.backgroundColor = UIColor.white
        self.emailTextField.autocapitalizationType = UITextAutocapitalizationType.none
        self.emailTextField.autocorrectionType = UITextAutocorrectionType.no
        self.emailTextField.font = UIFont(name: "Museo-300", size: 14)!
        self.emailTextField.tintColor = UIColor.aecColor()
        self.emailTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        self.emailTextField.placeholder = NSLocalizedString("Email", comment: "")
        self.emailTextField.returnKeyType = UIReturnKeyType.next
        self.emailTextField.keyboardType = UIKeyboardType.emailAddress
        self.emailTextField.layer.cornerRadius = 5
        self.emailTextField.layer.masksToBounds = true
        self.emailTextField.delegate = self

        self.passwordTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
        self.passwordTextField.leftViewMode = UITextFieldViewMode.always
        self.passwordTextField.backgroundColor = UIColor.white
        self.passwordTextField.autocorrectionType = UITextAutocorrectionType.no
        self.passwordTextField.font = UIFont(name: "Museo-300", size: 14)!
        self.passwordTextField.tintColor = UIColor.aecColor()
        self.passwordTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        self.passwordTextField.placeholder = NSLocalizedString("Senha", comment: "")
        self.passwordTextField.returnKeyType = UIReturnKeyType.done
        self.passwordTextField.keyboardType = UIKeyboardType.asciiCapable
        self.passwordTextField.isSecureTextEntry = true
        self.passwordTextField.layer.cornerRadius = 5
        self.passwordTextField.layer.masksToBounds = true
        self.passwordTextField.delegate = self
        
        self.loginButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.loginButton.setTitleColor(UIColor.white, for: UIControlState())
        self.loginButton.setBackgroundImage(UIImage.color(UIColor.aecColor()), for: UIControlState())
        self.loginButton.setTitle(NSLocalizedString("Entrar", comment:""), for: UIControlState())
        self.loginButton.addTarget(self, action: #selector(self.tapLoginButton), for: UIControlEvents.touchUpInside)
        self.loginButton.layer.cornerRadius = 5
        self.loginButton.layer.masksToBounds = true
        
        self.forgotPasswordButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.forgotPasswordButton.setTitleColor(UIColor.aecColor(), for: UIControlState())
        self.forgotPasswordButton.setTitle(NSLocalizedString("Esqueci minha senha", comment:""), for: UIControlState())
        self.forgotPasswordButton.addTarget(self, action: #selector(self.tapForgotPasswordButton), for: UIControlEvents.touchUpInside)
        self.forgotPasswordButton.layer.cornerRadius = 5
        self.forgotPasswordButton.layer.masksToBounds = true
        
        self.view.layer.addSublayer(Util.shapeLayer(self.loginButton.frame.origin.x, fromY: self.loginButton.frame.origin.y + 104, toX: UIScreen.main.bounds.width - 20, toY: self.loginButton.frame.origin.y + 104, width: 0.5, color: UIColor.white))
        
        self.label.textColor = UIColor.white
        self.label.font = UIFont(name: "Museo-500", size: 16)!
        self.label.textAlignment = NSTextAlignment.center
        self.label.text = NSLocalizedString("Ainda não tem uma conta?", comment: "")

        self.signUpButton.titleLabel!.font = UIFont(name: "Museo-500", size: 14)!
        self.signUpButton.setTitleColor(UIColor.white, for: UIControlState())
        self.signUpButton.setBackgroundImage(UIImage.color(UIColor.aecColor()), for: UIControlState())
        self.signUpButton.setTitle(NSLocalizedString("Crie seu perfil agora!", comment:""), for: UIControlState())
        self.signUpButton.addTarget(self, action: #selector(self.tapSignUpButton), for: UIControlEvents.touchUpInside)
        self.signUpButton.layer.cornerRadius = 5
        self.signUpButton.layer.masksToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didRegisterAccount), name:NSNotification.Name(rawValue: "registerAccount"), object: nil)
    }
    
    func tapView() {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    func tapLoginButton() {
        self.tapView()
        if self.validateFields() {
            let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
            hud.mode = MBProgressHUDMode.indeterminate
            hud.label.text = NSLocalizedString("Aguarde...", comment: "")
            hud.show(animated: true)
            Util.generateToken(email: self.emailTextField.text!, password: self.passwordTextField.text!)
            App.shared.login { (error) in
                hud.hide(animated: true)
                if error == nil {
                    self.emailTextField.text = nil
                    self.passwordTextField.text = nil
                    self.login()
                } else {
                    if error!["message"]! == "You have to login with proper credentials" {
                        Util.showAlert(NSLocalizedString("Login incorreto", comment: ""), message: "Verifique o email e senha inseridos.", cancel: NSLocalizedString("OK", comment: ""))
                    } else {
                        Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                    }
                }
            }
        }
    }
    
    func tapSignUpButton() {
        self.navigationController?.pushViewController(WebViewController(title: NSLocalizedString("Criar Perfil", comment: ""), url: GlobalConstants.registerURL), animated: true)
    }
    
    func login() {
        if Owner.shared.filter == nil {
            Owner.shared.filter = Filter()
        }
        UIApplication.shared.isStatusBarHidden = false
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(TabBarController(), animated: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            self.passwordTextField.resignFirstResponder()
            UIApplication.topViewController()!.dismiss(animated: true, completion: nil)
            self.tapLoginButton()
        }
        return false
    }
    
    func validateFields() -> Bool {
        if self.emailTextField.text == nil || self.emailTextField.text == "" || self.passwordTextField == nil || self.emailTextField.text == "" {
            Util.showAlert(NSLocalizedString("Atenção", comment: ""), message: NSLocalizedString("Preencha todos os campos", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
            return false
        }
        return true
    }
    
    func validateEmail(_ string: NSString) -> Bool {
        let emailRegex = NSString(string: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: string)
    }
    
    
    func tapForgotPasswordButton() {
        self.navigationController?.pushViewController(WebViewController(title: NSLocalizedString("Envio de  Senha", comment: ""), url: GlobalConstants.forgotPasswordURL), animated: true)
    }
    
    func didRegisterAccount() {
        _ = UIApplication.topViewController()?.navigationController?.popViewController(animated: false)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
        self.login()
    }
}
