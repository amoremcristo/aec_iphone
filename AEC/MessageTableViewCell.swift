//
//  MessageTableViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD

class MessageTableViewCell: UITableViewCell {
    
    fileprivate var message: Message!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var nicknameLabel: UILabel!
    fileprivate var messageLabel: UILabel!
    fileprivate var dateLabel: UILabel!
    fileprivate var dateImageView: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.avatarImageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 44, height: 44))
        self.nicknameLabel = UILabel()
        self.messageLabel = UILabel()
        self.dateLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width - 70, y: 0, width: 60, height: MessageTableViewCell.height()))
        self.dateImageView = UIImageView(image: #imageLiteral(resourceName: "mensagemRecebidaIcone").color(UIColor.aecColor()))
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.image = nil
        self.dateImageView.isHidden = true
        self.dateLabel.textColor = UIColor.gray
        self.dateLabel.font = UIFont(name: "Museo-300", size: 10)
    }
    
    func layout() {
        self.contentView.addSubview(self.avatarImageView)
        self.contentView.addSubview(self.nicknameLabel)
        self.contentView.addSubview(self.messageLabel)
        self.contentView.addSubview(self.dateLabel)
        self.contentView.addSubview(self.dateImageView)
        
        self.avatarImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.avatarImageView.backgroundColor = UIColor.darkGray
        self.avatarImageView.layer.cornerRadius = 22
        self.avatarImageView.layer.masksToBounds = true
        
        self.nicknameLabel.font = UIFont(name: "Museo-500", size: 14)
        
        self.messageLabel.textColor = UIColor.gray
        self.messageLabel.font = UIFont(name: "Museo-300", size: 12)
        
        self.dateLabel.textColor = UIColor.gray
        self.dateLabel.font = UIFont(name: "Museo-300", size: 10)
        self.dateLabel.textAlignment = NSTextAlignment.center
        
        self.dateImageView.isHidden = true
    }
    
    func layout(_ message: Message) {
        self.message = message
        
        self.avatarImageView.load(self.message.user!.avatar, thumb: true, placeholderImage: nil)
        
        self.nicknameLabel.text = self.message.user.nickname
        self.nicknameLabel.sizeToFit()
        self.nicknameLabel.frame = CGRect(x: 64, y: 15, width: UIScreen.main.bounds.width - 134, height: self.nicknameLabel.bounds.height)
        
        self.messageLabel.text = self.message.message
        self.messageLabel.sizeToFit()
        self.messageLabel.frame = CGRect(x: 64, y: self.nicknameLabel.frame.origin.y + self.nicknameLabel.bounds.height + 3, width: UIScreen.main.bounds.width - 134, height: self.messageLabel.bounds.height)

        self.dateLabel.text = self.message.date.formatTimeAgo()
        
        if self.message.unread! {
            self.dateImageView.isHidden = false
            self.dateImageView.center = self.contentView.center
            self.dateImageView.frame.origin = CGPoint(x: self.dateLabel.frame.origin.x - self.dateImageView.bounds.width - 2.5, y: self.dateImageView.frame.origin.y)
            
            self.dateLabel.textColor = UIColor.aecColor()
            self.dateLabel.font = UIFont(name: "Museo-500", size: 10)
        }
    }
    
    class func height() -> CGFloat {
        return 64
    }
}
