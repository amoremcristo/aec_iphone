//
//  UserCollectionViewCell.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import MBProgressHUD

class UserCollectionViewCell: UICollectionViewCell {
    
    fileprivate var user: User!
    fileprivate var avatarImageView: UIImageView!
    fileprivate var nicknameLabel: UILabel!
    fileprivate var detailsLabel: UILabel!
    fileprivate var messageButton: UIButton!
    fileprivate var favoriteButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.avatarImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.contentView.bounds.width, height: self.contentView.bounds.height - 40))
        self.nicknameLabel = UILabel()
        self.detailsLabel = UILabel()
        self.messageButton = UIButton(frame: CGRect(x: 0, y: self.contentView.bounds.height - 40, width: self.contentView.bounds.width / 2, height: 40))
        self.favoriteButton = UIButton(frame: CGRect(x: self.contentView.bounds.width / 2, y: self.contentView.bounds.height - 40, width: self.contentView.bounds.width / 2, height: 40))
        self.layout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.image = nil
    }
    
    func layout() {
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.1
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 2.5).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.contentView.backgroundColor = UIColor.white
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 2.5
        
        self.contentView.addSubview(self.avatarImageView)
        self.contentView.addSubview(self.nicknameLabel)
        self.contentView.addSubview(self.detailsLabel)
        self.contentView.addSubview(self.messageButton)
        self.contentView.addSubview(self.favoriteButton)
        
        self.avatarImageView.contentMode = UIViewContentMode.scaleAspectFill
        self.avatarImageView.backgroundColor = UIColor.darkGray
        
        self.nicknameLabel.textColor = UIColor.white
        self.nicknameLabel.font = UIFont(name: "Museo-500", size: 18)
        self.nicknameLabel.layer.shadowColor = UIColor.black.cgColor
        self.nicknameLabel.layer.shadowOpacity = 0.6
        self.nicknameLabel.layer.shadowRadius = 5
        self.nicknameLabel.layer.shadowOffset = CGSize.zero
        self.nicknameLabel.layer.shouldRasterize = true
        self.nicknameLabel.layer.rasterizationScale = UIScreen.main.scale
        
        self.detailsLabel.textColor = UIColor.white
        self.detailsLabel.font = UIFont(name: "Museo-300", size: 12)
        self.detailsLabel.layer.shadowColor = UIColor.black.cgColor
        self.detailsLabel.layer.shadowOpacity = 0.6
        self.detailsLabel.layer.shadowRadius = 5
        self.detailsLabel.layer.shadowOffset = CGSize.zero
        self.detailsLabel.layer.shouldRasterize = true
        self.detailsLabel.layer.rasterizationScale = UIScreen.main.scale
        
        self.messageButton.setImage(UIImage(named: "btMensagem")?.color(UIColor.aecColor()), for: UIControlState())
        self.messageButton.setImage(UIImage(named: "btMensagem")?.color(UIColor.aecColor().lighterColor()), for: UIControlState.highlighted)
        self.messageButton.addTarget(self, action: #selector(self.tapMessageButton), for: .touchUpInside)
        
        self.favoriteButton.setImage(UIImage(named: "btFavoritoNormal")?.color(UIColor.aecColor()), for: .normal)
        self.favoriteButton.setImage(UIImage(named: "btFavoritoSelecionado")?.color(UIColor.aecColor()), for: .highlighted)
        self.favoriteButton.setImage(UIImage(named: "btFavoritoSelecionado")?.color(UIColor.aecColor()), for: .selected)
        self.favoriteButton.setImage(UIImage(named: "btFavoritoNormal")?.color(UIColor.aecColor()), for: [.selected, .highlighted])
        self.favoriteButton.addTarget(self, action: #selector(self.tapFavoriteButton), for: .touchUpInside)
    }
    
    func layout(_ user: User) {
        self.user = user

        self.avatarImageView.load(self.user!.avatar, thumb: true, placeholderImage: nil)
        
        self.nicknameLabel.text = self.user.nickname
        self.nicknameLabel.sizeToFit()
        
        self.detailsLabel.text = self.user.details
        self.detailsLabel.sizeToFit()
        self.detailsLabel.frame = CGRect(x: 5, y: self.avatarImageView.bounds.height - 5 - self.detailsLabel.bounds.height, width: self.contentView.bounds.width - 10, height: self.detailsLabel.bounds.height)
        
        self.nicknameLabel.frame = CGRect(x: 5, y: self.detailsLabel.frame.origin.y - self.nicknameLabel.bounds.height, width: self.contentView.bounds.width - 10, height: self.nicknameLabel.bounds.height)
    }
    
    func tapMessageButton() {
        let viewController = ChatViewController(user: self.user)
        viewController.hidesBottomBarWhenPushed = true
        UIApplication.topViewController()!.navigationController!.pushViewController(viewController, animated: true)
    }
    
    func tapFavoriteButton(_ sender: UIButton) {
        let hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
        hud.mode = MBProgressHUDMode.indeterminate
        hud.label.text = NSLocalizedString("Aguarde...", comment: "")
        hud.show(animated: true)
        if user.favorite == false {
            self.user.favorite({ (error) -> Void in
                if error == nil {
                    Util.showAlert(NSLocalizedString("Aguarde", comment: ""), message: NSLocalizedString("Seu pedido está sendo processado pelo sistema. Em breve sua lista de Favoritos será atualizada.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                } else {
                    Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                }
                hud.hide(animated: true)
            })
        } else {
            self.user.unfavorite({ (error) -> Void in
                if error == nil {
                    Util.showAlert(NSLocalizedString("Aguarde", comment: ""), message: NSLocalizedString("Seu pedido está sendo processado pelo sistema. Em breve sua lista de Favoritos será atualizada.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                } else {
                    Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
                }
                hud.hide(animated: true)
            })
        }
    }
    
    class func height() -> CGFloat {
        return 220
    }
    
}
