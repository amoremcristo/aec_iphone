//
//  PlanViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit
import StoreKit
import MBProgressHUD

class PlanViewController: UITableViewController, SKRequestDelegate, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    fileprivate var items: [SKProduct]!
    fileprivate var itemsHeight: CGFloat!
    fileprivate var hud: MBProgressHUD!
    

    init() {
        super.init(style: .plain)
        self.items = [SKProduct]()
        self.itemsHeight = PlanTableViewCell.height()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout()
        self.loadItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func layout() {
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Planos", comment: "")
        self.navigationItem.leftBarButtonItems =  [UIBarButtonItem.close(self, action: #selector(self.tapCloseButton))]
        self.view.backgroundColor = UIColor.backgroundColor()
        
        self.tableView.contentInset = UIEdgeInsets(top: 2.5, left: 0, bottom: 2.5, right: 0)
        self.tableView.backgroundColor = UIColor.backgroundColor()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.alwaysBounceVertical = true
        self.tableView.register(PlanTableViewCell.self, forCellReuseIdentifier: "cell")
        
        SKPaymentQueue.default().add(self)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.itemsHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PlanTableViewCell
        cell.layout(self.items[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = self.items[indexPath.row]
        if SKPaymentQueue.canMakePayments() {
            SKPaymentQueue.default().add(SKPayment(product: product))
        } else {
            Util.showAlert(NSLocalizedString("Sua compra não foi realizada", comment: ""), message: NSLocalizedString("Esta conta não pode realizar compras. Por favor, acesse com outro Apple ID.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.hud.hide(animated: true)
        self.items = response.products
        self.items.sort {
            return Float($0.price) < Float($1.price)
        }
        self.tableView.reloadData()
    }

    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                SKPaymentQueue.default().finishTransaction(transaction)
                self.processPayment(transaction: transaction)
                break
            case .restored:
                self.hud.hide(animated: true)
                Util.showAlert(NSLocalizedString("Aviso", comment: ""), message: NSLocalizedString("Esta compra dentro do app já foi efetuada. Aguarde enquanto estamos completando a transação.", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .failed:
                self.hud.hide(animated: true)
                Util.showAlert(NSLocalizedString("Atenção", comment: ""), message: transaction.error?.localizedDescription, cancel: NSLocalizedString("OK", comment: ""))
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .deferred:
                break
            case .purchasing:
                //                self.hud.showAnimated(<#T##animated: Bool##Bool#>)
                //                self.hud.showInView(self.navigationController?.view, animated: true)
                break
            }
        }
    }
    
    func loadItems() {
        self.hud = MBProgressHUD.showAdded(to: UIApplication.topViewController()!.navigationController!.view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.label.text = NSLocalizedString("Aguarde...", comment: "")
        self.hud.show(animated: true)
        App.shared.products { (items, error) in
            if error == nil {
                let request = SKProductsRequest(productIdentifiers: items!)
                request.delegate = self
                request.start()
            } else {
                self.hud.hide(animated: true)
                Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
            }
        }
    }
    func processPayment(transaction: SKPaymentTransaction) {
        App.shared.processPayment(transaction: transaction) { (error) in
            self.hud.hide(animated: true)
            if error == nil {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didBecomePremium"), object: nil)
                self.dismiss(animated: true, completion: { 
                    Util.showAlert(NSLocalizedString("Sucesso", comment: ""), message: NSLocalizedString("Parabéns! Agora você é um usuário premium!", comment: ""), cancel: NSLocalizedString("OK", comment: ""))
                })
            } else {
                Util.showAlert(error!["title"]!, message: error!["message"]!, cancel: NSLocalizedString("OK", comment: ""))
            }
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        self.hud.hide(animated: true)
        Util.showAlert(NSLocalizedString("Atenção", comment: ""), message: error.localizedDescription, cancel: NSLocalizedString("OK", comment: ""))
    }
    
    func tapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
}
