//
//  FilterViewController.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class FilterViewController: UITableViewController {
    
    static let sharedInstance = FilterViewController()
    
    var femaleButton: UIButton!
    var maleButton: UIButton!
    var bothButton: UIButton!
    var minAgeSlider: UISlider!
    var minAgeLabel: UILabel!
    var maxAgeSlider: UISlider!
    var maxAgeLabel: UILabel!
    var photoSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.maleButton = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width / 3, height: 88))
        self.femaleButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width / 3, y: 0, width: UIScreen.main.bounds.width / 3, height: 88))
        self.bothButton = UIButton(frame: CGRect(x: UIScreen.main.bounds.width / 3 * 2, y: 0, width: UIScreen.main.bounds.width / 3, height: 88))
        self.minAgeSlider = UISlider(frame: CGRect(x: 50, y: 0, width: UIScreen.main.bounds.width - 100, height: 44))
        self.minAgeLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width - 50, y: 0, width: 50, height: 44))
        self.maxAgeSlider = UISlider(frame: CGRect(x: 50, y: 0, width: UIScreen.main.bounds.width - 100, height: 44))
        self.maxAgeLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width - 50, y: 0, width: 50, height: 44))
        self.photoSwitch = UISwitch()
        self.layout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init() {
        super.init(style: UITableViewStyle.grouped)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layout() {
        self.view.backgroundColor = UIColor.backgroundColor()
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationItem.title = NSLocalizedString("Filtro", comment: "")
        self.navigationItem.leftBarButtonItems =  [UIBarButtonItem.cancel(self, action: #selector(self.tapCancelButton))]
        self.navigationItem.rightBarButtonItems =  [UIBarButtonItem.apply(self, action: #selector(self.tapApplyButton))]
        
        
        self.maleButton.setImage(UIImage(named: "filtroHomemNormal"), for: UIControlState())
        self.maleButton.setImage(UIImage(named: "filtroHomemSelecionado")?.color(UIColor.aecColor()), for: UIControlState.selected)
        self.maleButton.setTitle(NSLocalizedString("Homem", comment: "").uppercased(), for: UIControlState())
        self.maleButton.setTitleColor(UIColor.gray, for: UIControlState())
        self.maleButton.setTitleColor(UIColor.aecColor(), for: .selected)
        self.maleButton.titleLabel?.font = UIFont(name: "Museo-500", size: 12)
        self.maleButton.addTarget(self, action: #selector(self.tapMaleButton), for: .touchUpInside)
        
        self.femaleButton.setImage(UIImage(named: "filtroMulherNormal"), for: UIControlState())
        self.femaleButton.setImage(UIImage(named: "filtroMulherSelecionado")?.color(UIColor.aecColor()), for: UIControlState.selected)
        self.femaleButton.setTitle(NSLocalizedString("Mulher", comment: "").uppercased(), for: UIControlState())
        self.femaleButton.setTitleColor(UIColor.gray, for: UIControlState())
        self.femaleButton.setTitleColor(UIColor.aecColor(), for: .selected)
        self.femaleButton.titleLabel?.font = UIFont(name: "Museo-500", size: 12)
        self.femaleButton.addTarget(self, action: #selector(self.tapFemaleButton), for: .touchUpInside)

        self.bothButton.setImage(UIImage(named: "filtroAmbosNormal"), for: UIControlState())
        self.bothButton.setImage(UIImage(named: "filtroAmbosSelecionado")?.color(UIColor.aecColor()), for: UIControlState.selected)
        self.bothButton.setTitle(NSLocalizedString("Ambos", comment: "").uppercased(), for: UIControlState())
        self.bothButton.setTitleColor(UIColor.gray, for: UIControlState())
        self.bothButton.setTitleColor(UIColor.aecColor(), for: .selected)
        self.bothButton.titleLabel?.font = UIFont(name: "Museo-500", size: 12)
        self.bothButton.addTarget(self, action: #selector(self.tapBothButton), for: .touchUpInside)
        switch Owner.shared.filter!.gender! {
        case 1:
            self.maleButton.isSelected = true
            break
        case 0:
            self.femaleButton.isSelected = true
            break
        default:
            self.bothButton.isSelected = true
            break
        }

        let spacing: CGFloat = 6.0
        var imageSize: CGSize = self.maleButton.imageView!.image!.size
        self.maleButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        var labelString = NSString(string: self.maleButton.titleLabel!.text!)
        var titleSize = labelString.size(attributes: [NSFontAttributeName: self.maleButton.titleLabel!.font])
        self.maleButton.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        var edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.maleButton.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
        
        imageSize = self.femaleButton.imageView!.image!.size
        self.femaleButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        labelString = NSString(string: self.femaleButton.titleLabel!.text!)
        titleSize = labelString.size(attributes: [NSFontAttributeName: self.femaleButton.titleLabel!.font])
        self.femaleButton.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.femaleButton.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
        
        imageSize = self.bothButton.imageView!.image!.size
        self.bothButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        labelString = NSString(string: self.bothButton.titleLabel!.text!)
        titleSize = labelString.size(attributes: [NSFontAttributeName: self.bothButton.titleLabel!.font])
        self.bothButton.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.bothButton.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
        
        self.minAgeSlider.minimumValue = 18
        self.minAgeSlider.maximumValue = 90
        self.minAgeSlider.value = Float(Owner.shared.filter!.minAge)
        self.minAgeSlider.tintColor = UIColor.aecColor()
        self.minAgeSlider.addTarget(self, action: #selector(self.changeMinAgeSlider(_:)), for: UIControlEvents.valueChanged)
        
        self.minAgeLabel.font = UIFont(name: "Museo-500", size: 12)
        self.minAgeLabel.textAlignment = NSTextAlignment.center
        self.minAgeLabel.text = "\(Int(self.minAgeSlider.value))"
        self.minAgeLabel.textColor = .aecColor()
        
        self.maxAgeSlider.minimumValue = 18
        self.maxAgeSlider.maximumValue = 90
        self.maxAgeSlider.value = Float(Owner.shared.filter!.maxAge)
        self.maxAgeSlider.tintColor = UIColor.aecColor()
        self.maxAgeSlider.addTarget(self, action: #selector(self.changeMaxAgeSlider(_:)), for: UIControlEvents.valueChanged)
        
        self.maxAgeLabel.font = UIFont(name: "Museo-500", size: 12)
        self.maxAgeLabel.textAlignment = NSTextAlignment.center
        self.maxAgeLabel.text = "\(Int(self.maxAgeSlider.value))"
        self.maxAgeLabel.textColor = .aecColor()
        
        self.photoSwitch.frame = CGRect(x: UIScreen.main.bounds.width - self.photoSwitch.bounds.width - 15, y: 7, width: self.photoSwitch.bounds.width, height: self.photoSwitch.bounds.height)
        self.photoSwitch.onTintColor = UIColor.aecColor()
        self.photoSwitch.addTarget(self, action: #selector(self.changePhotoSwitch(_:)), for: UIControlEvents.valueChanged)
        self.photoSwitch.isOn = Owner.shared.filter!.photo
        
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.font = UIFont(name: "Museo-500", size: 14)
        headerView.textLabel?.textColor = UIColor.black
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Procuro", comment: "").uppercased()
        case 1:
            return NSLocalizedString("Idade", comment: "").uppercased()
        case 2:
            return NSLocalizedString("Fotos", comment: "").uppercased()
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 2
        default:
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 88
        default:
            return 44
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.font = UIFont(name: "Museo-500", size: 12)
        cell.textLabel?.textColor = UIColor.gray
        switch indexPath.section {
        case 0:
            cell.contentView.addSubview(self.maleButton)
            cell.contentView.addSubview(self.femaleButton)
            cell.contentView.addSubview(self.bothButton)
            break
        case 1:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text =  NSLocalizedString("Min", comment: "")
                cell.contentView.addSubview(self.minAgeSlider)
                cell.contentView.addSubview(self.minAgeLabel)
                break
            case 1:
                cell.textLabel?.text =  NSLocalizedString("Max", comment: "")
                cell.contentView.addSubview(self.maxAgeSlider)
                cell.contentView.addSubview(self.maxAgeLabel)
                break
            default:
                break
            }
        case 2:
            cell.textLabel?.text = NSLocalizedString("Com fotos", comment: "")
            cell.contentView.addSubview(self.photoSwitch)
            break
        default:
            break
        }
        return cell
    }
    
    func tapMaleButton(_ sender: UIButton) {
        self.unselectButtons()
        self.maleButton.isSelected = true
        Owner.shared.filter?.isDefault = false
        Owner.shared.filter?.gender = 1
    }
    
    func tapFemaleButton(_ sender: UIButton) {
        self.unselectButtons()
        self.femaleButton.isSelected = true
        Owner.shared.filter?.isDefault = false
        Owner.shared.filter?.gender = 0
    }
    
    func tapBothButton(_ sender: UIButton) {
        self.unselectButtons()
        self.bothButton.isSelected = true
        Owner.shared.filter?.isDefault = false
        Owner.shared.filter?.gender = 2
    }
    
    func unselectButtons() {
        self.maleButton.isSelected = false
        self.femaleButton.isSelected = false
        self.bothButton.isSelected = false
    }
    
    func changeMinAgeSlider(_ sender: UISlider) {
        self.minAgeLabel.text = "\(Int(sender.value))"
        Owner.shared.filter?.isDefault = false
        Owner.shared.filter!.minAge = Int(sender.value)
    }
    
    func changeMaxAgeSlider(_ sender: UISlider) {
        self.maxAgeLabel.text = "\(Int(sender.value))"
        Owner.shared.filter?.isDefault = false
        Owner.shared.filter!.maxAge = Int(sender.value)
    }
    
    func changePhotoSwitch(_ sender: UISwitch) {
        Owner.shared.filter?.isDefault = false
        Owner.shared.filter!.photo = sender.isOn
    }
    
    func tapCancelButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tapApplyButton() {
        Owner.shared.save()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "didSelectFilter"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}
