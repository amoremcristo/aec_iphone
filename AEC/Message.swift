//
//  Message.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class Message {
    
    var id: NSInteger!
    var user: User!
    var message: String?
    var unread: Bool?
    var date: Date!
    var inactive: Bool?
    
    init(dictionary: NSDictionary) {
        self.parse(dictionary)
    }
    
    func parse(_ dictionary: NSDictionary) {
        if (dictionary["id"] as? NSInteger) != nil {
            self.id = dictionary["id"] as! NSInteger
        }
        if (dictionary["user_id"] as? NSInteger) != nil {
            self.user = User(id: dictionary["user_id"] as! NSInteger, nickname: dictionary["title"] as! String, avatar: Picture(dictionary: dictionary["pic"] as! NSDictionary))
        }
        if (dictionary["subtitle"] as? String) != nil {
            self.message = dictionary["subtitle"] as? String
        }
        self.unread = dictionary["unread"] as? Bool
        self.inactive = dictionary["inactive"] as? Bool
        self.date = DateFormatter().date(from: dictionary["date"] as! String)
    }
}

