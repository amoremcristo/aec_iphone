//
//  Picture.swift
//  AEC
//
//  Created by Marcelo Cotrim on 11/01/17.
//
//

import UIKit

class Picture: NSObject, NSCoding {
    
    var URL: String?
    var thumbURL: String?
    var image: UIImage?
    
    required init?(coder aDecoder: NSCoder) {
        self.URL = aDecoder.decodeObject(forKey: "URL") as? String
        self.thumbURL = aDecoder.decodeObject(forKey: "thumbURL") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.URL, forKey: "URL")
        aCoder.encode(self.thumbURL, forKey: "thumbURL")
    }
    
    init(dictionary: NSDictionary) {
        super.init()
        self.URL = dictionary["large"] as? String
        self.thumbURL = dictionary["thumb"] as? String
    }
    
    init(URL: String) {
        super.init()
        self.URL = URL
        self.thumbURL = URL
    }
    
    init(image: UIImage) {
        super.init()
        self.image = image
    }
}
